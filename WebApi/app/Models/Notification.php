<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Nonstandard\UuidV6;

class Notification extends Model
{
    use HasFactory, Uuids;

    public const APPOINTMENT_CREATED = "APPOINTMENT_CREATED";
    public const APPOINTMENT_REMINDER = "APPOINTMENT_REMINDER";
    public const INFORMATION = "INFORMATION";
}
