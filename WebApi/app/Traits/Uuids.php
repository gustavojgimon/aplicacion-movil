<?php

namespace App\Traits;

use Ramsey\Uuid\Nonstandard\UuidV6;

trait Uuids
{
    /**
     * Boot function from Laravel.
     */
    protected static function boot() {
        parent::boot();
        self::creating(function ($model) {
            if (!app()->runningInConsole() || empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = UuidV6::uuid6()->toString();
            }
        });
    }

   /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

   /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }
}