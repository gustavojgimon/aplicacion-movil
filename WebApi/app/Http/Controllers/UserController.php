<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Get user information profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserProfile()
    {
        $user = Auth::user();
        $rrhh_personal = DB::connection("mysql_personal")->select(
            'SELECT 

            rp.per_codigo as Codigo, 
            rp.per_cedula as Cedula, 
            rp.per_nombres as Nombres, 
            rp.per_apellidos as Apellidos, 
            st.st_descripcion as Estatus, 
            rpd.perdat_correo as Correo, 
            rn.nom_nombre as Nomina, 
            rc.car_nombre as Cargo, 
            vr.Nombre as Vicerectorado, 
            dj.Departamento as Departamento 

            FROM rrhh_personal rp inner join 
            rrhh_personal_datosp rpd on rpd.perdat_percodigo=rp.per_codigo inner join 
            rrhh_personal_cargo rpc on rpc.perc_percodigo=rp.per_codigo inner join 
            rrhh_nomina rn on rn.nom_codigo=rpc.perc_nomcodigo inner join 
            rrhh_cargo rc on rc.car_codigo=rpc.perc_carcodigo inner join
            rrhh_personal_ubica rpu on rpu.peru_percodigo= :perCodigo1 inner join 
            vicerectorado vr on vr.id=rpu.peru_vicerrec inner join 
            asignar_personal_dep dep on dep.per_codigo= :perCodigo2 inner join
            asignar_departamentos_sub ds on ds.id=dep.Coordinacion inner join
            asignar_departamentos_jerarquica dj on dj.id=ds.SubDepartamento inner join 
            status st on st.st_codigo=rp.per_status where rp.per_codigo= :perCodigo3 and rpc.perc_status=1', 
            
            [   "perCodigo1" => $user->per_codigo,
                "perCodigo2" => $user->per_codigo,
                "perCodigo3" => $user->per_codigo]);
        return $rrhh_personal;
    }

    /**
     * Get family burden information
     *
     * @return \Illuminate\Http\Response
     */
    public function getFamilyBurden()
    {
        $user = Auth::user();
        $rrhh_cargafam_tit = DB::connection('mysql_personal')->select(
        'SELECT 
        rcf.carfam_cedula as DNI, 
        CONCAT(rcf.carfam_nombre,rcf.carfam_apellido) as Names,
        s.sex_descripcion as Gender,
        rcf.carfam_fecnac as BirthDate,
        p.par_descripcion as Relationship
        FROM rrhh_cargafam_tit rcft 
        inner join rrhh_cargafam rcf on rcf.carfam_codigo=rcft.cargt_cargcodigo 
        inner join tools_parentesco p on p.par_codigo=rcft.cargt_parentesco 
        inner join tools_sexo s on s.sex_codigo=rcf.carfam_sexo
        where rcft.cargt_percodigo= :perCodigo', ["perCodigo"=>$user->per_codigo]);
        // $rrhh_cargafam_tit = array(
        // array('DNI' => '2719922','Names' => 'GIMON MAXIMO','Gender' => 'Masculino','BirthDate' => '1946-12-15','Relationship' => 'PADRE'),
        // array('DNI' => '9382812','Names' => 'VILLENA AURA ROSA','Gender' => 'Femenino','BirthDate' => '1963-12-07','Relationship' => 'MADRE'),
        // array('DNI' => '20963746','Names' => 'GUZZO GOMEZ ANDRINA CAROLINA','Gender' => 'Femenino','BirthDate' => '1991-09-26','Relationship' => 'CONCUBINO (A)')
        // );
        return $rrhh_cargafam_tit;
    }

    /**
     * Get medical appointments information
     *
     * @return \Illuminate\Http\Response
     */
    public function getMedicalAppointments()
    {
        //tOdO not has query.
       $appointments = array(
        array(
            'Id' => '989278ef-aade-407e-b934-df407ccfc8c0',
            'Specialization' => 'Ginecología',
            'DoctorName' => 'Ricardo Tovar',
            'Date' => '18-02-2021',
            'Location' => 'Centro Clínico del Valle'),
            array(
            'Id' => 'd5afbe02-3a90-4713-82ef-412cf6f39b29',
            'Specialization' => 'Odontología',
            'DoctorName' => 'Greissy Mora',
            'Date' => '15-02-2021',
            'Location' => 'Clínica el Pilar')
        );

        return $appointments;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalAppointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function getMedicalAppointment($appointment)
    {
        //tOdO not has query.
       $appointment = array(
            'Id' => '989278ef-aade-407e-b934-df407ccfc8c0',
            'Specialization' => 'Ginecología',
            'DoctorName' => 'Ricardo Tovar',
            'Date' => '18-02-2021',
            'Location' => 'Centro Clínico del Valle');

        return $appointment;
    }
}
