<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;
use LdapRecord\Container;

class AuthController extends Controller
{
    public function login(Request $request){
         $messages = [
            'email.required' => 'El correo electronico es requerido',
            'password.required' => 'La contrase;a es requerida',
            'device_id.required' => 'El ID del dispositivo es requerido'
        ];
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'device_id' => 'required'
        ], $messages);

        if (Auth::attempt(['uid' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            if ($user->per_codigo == null){
                $query = Container::getDefaultConnection()->query();
                $ldapUser = $query->where('mail', $user->email)->first();
                $cedula = $ldapUser['phpgwisdnphonenumber'][0];
                $data = DB::connection("mysql_personal")->select('select per_codigo from rrhh_personal where per_cedula = ?', [$cedula]);
                $user->per_codigo = $data[0]->per_codigo;
                $user->save();
            }
            $user->tokens()->where('name', $request->device_id)->delete();
            return response()->json([
                "access_token" => $user->createToken($request->device_id)->plainTextToken,
                "token_type" => "Bearer"
            ]);
        }else{
            throw ValidationException::withMessages([
                'credentials' => [Lang::get('auth.failed')]
            ]);
        }
    }

    public function logout(Request $request){
        $id = explode("|", $request->bearerToken())[0];
        return $request->user()->tokens()->where('id', $id)->delete();
    }
}
