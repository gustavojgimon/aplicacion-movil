<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Log;
use LdapRecord\Laravel\Testing\DirectoryEmulator;
use LdapRecord\Models\OpenLDAP\User;
use LdapRecord\Testing\DirectoryFake;
use Tests\TestCase;

class LdapAuthenticationTest extends TestCase
{
    public function test_login_ldap()
    {
        $this->post('/api/v1/login', [
            'email' => 'newton@ldap.forumsys.com',
            'password' => 'password',
            'device_id' => '4fc08a9c-dad8-4892-9416-a0666cf66e5f'
        ], [
            "Accept" => "application/json"
        ])->assertSeeText('Bearer');

        $this->post('/api/v1/login', [
            'email' => 'invalid',
            'password' => 'password',
            'device_id' => 'bd5e80f3-ca18-4c3c-b65a-d70c92e75e79'
        ], [
            "Accept" => "application/json"
        ])->assertSeeText('El correo debe ser una direccion valida.');

        $this->post('/api/v1/login', [
            'email' => 'invalid@correo.com',
            'password' => 'password',
            'device_id' => 'bd5e80f3-ca18-4c3c-b65a-d70c92e75e79'
        ], [
            "Accept" => "application/json"
        ])->assertSeeText('Las credenciales no coinciden con nuestros registros.');
    }
}
