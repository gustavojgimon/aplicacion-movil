<?php

namespace Tests\Feature;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use LdapRecord\Testing\DirectoryFake;
use Tests\TestCase;

class GetUserInfoTest extends TestCase
{
    /**
     * Get user information test
     *
     * @return void
     */
    public function test_GetUser()
    {
        $user = User::factory()->create();

        DirectoryFake::setup()->actingAs($user);
        Sanctum::actingAs(
            $user,
            ['*']
        );

        $response = $this->get('/api/v1/user');
        $response->assertOk();
    }

    /**
     * Log out test
     *
     * @return void
     */
    public function test_Logout()
    {
        $user = User::factory()->create();

        DirectoryFake::setup()->actingAs($user);
        Sanctum::actingAs(
            $user,
            ['*']
        );

        $response = $this->post('/api/v1/logout');

        $response->assertOk();
    }

    /**
     * Login test
     *
     * @return void
     */
    public function test_GetUiTestToken()
    {
        $response = $this->get('/api/v1/testToken');

        $response->assertOk();
    }
}
