<?php

namespace Database\Factories;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid,
            'user_id' => 1,
            'title' => $this->faker->text(40),
            'description' => $this->faker->text(100),
            'is_read' => $this->faker->boolean(80),
            'type' => $this->faker->randomElement([Notification::APPOINTMENT_CREATED, Notification::APPOINTMENT_REMINDER, Notification::INFORMATION]),
            'id_entity' => $this->faker->numberBetween(0, 100),
            'priority' => $this->faker->numberBetween(0, 1)
        ];
    }
}
