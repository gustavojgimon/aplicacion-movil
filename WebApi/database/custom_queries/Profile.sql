SELECT 

rp.per_codigo as Codigo, 
rp.per_cedula as Cedula, 
rp.per_nombres as Nombres, 
rp.per_apellidos as Apellidos, 
st.st_descripcion as Estatus, 
rpd.perdat_correo as Correo, 
rn.nom_nombre as Nomina, 
rc.car_nombre as Cargo, 
vr.Nombre as Vicerectorado, 
dj.Departamento as Departamento 

FROM rrhh_personal rp inner join 
rrhh_personal_datosp rpd on rpd.perdat_percodigo=rp.per_codigo inner join 
rrhh_personal_cargo rpc on rpc.perc_percodigo=rp.per_codigo inner join 
rrhh_nomina rn on rn.nom_codigo=rpc.perc_nomcodigo inner join 
rrhh_cargo rc on rc.car_codigo=rpc.perc_carcodigo inner join
rrhh_personal_ubica rpu on rpu.peru_percodigo=6983 inner join 
vicerectorado vr on vr.id=rpu.peru_vicerrec inner join 
asignar_personal_dep dep on dep.per_codigo=6983 inner join
asignar_departamentos_sub ds on ds.id=dep.Coordinacion inner join
asignar_departamentos_jerarquica dj on dj.id=ds.SubDepartamento inner join 
status st on st.st_codigo=rp.per_status where rp.per_codigo=6983 and rpc.perc_status=1