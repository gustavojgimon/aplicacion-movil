SELECT 
rcf.carfam_cedula as DNI, 
CONCAT(rcf.carfam_nombre,rcf.carfam_apellido) as Names,
s.sex_descripcion as Gender,
rcf.carfam_fecnac as BirthDate,
p.par_descripcion as Relationship
FROM rrhh_cargafam_tit rcft 
inner join rrhh_cargafam rcf on rcf.carfam_codigo=rcft.cargt_cargcodigo 
inner join tools_parentesco p on p.par_codigo=rcft.cargt_parentesco 
inner join tools_sexo s on s.sex_codigo=rcf.carfam_sexo
where rcft.cargt_percodigo=6983