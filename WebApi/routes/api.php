<?php

use App\Models\User as ModelsUser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use LdapRecord\Container;
use LdapRecord\Models\OpenLDAP\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function(){

    //  Route::get('/testLdap', function () {
	//     return User::first();
    //  });
    Route::get('/testToken', function () {
        $testToken = env('UI_TEST_TOKEN_APP', null);
        
        if (!$testToken){
            throw ValidationException::withMessages([
                'env' => ['UITest environment for app not configured & not available in prod'],
            ])->status(Response::HTTP_SERVICE_UNAVAILABLE);
        }

        $user = ModelsUser::factory()->create();

        return response()->json([
            "access_token" => $user->createToken('2b5bb8cd-b426-401f-8d5e-a1bd39557382')->plainTextToken,
            "token_type" => "Bearer"
        ]);
    });

    Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');
    Route::middleware('auth:sanctum')->post('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');
    Route::get('/user', [\App\Http\Controllers\UserController::class, 'getUserProfile'])->name('user_profile');
    Route::get('/familyburden', [\App\Http\Controllers\UserController::class, 'getFamilyBurden'])->name('familyburden');
    Route::get('/medicalappointments', [\App\Http\Controllers\UserController::class, 'getMedicalAppointments'])->name('medicalappointments');
    Route::get('/medicalappointment/{appointment}', [\App\Http\Controllers\UserController::class, 'getMedicalAppointment'])->name('medicalappointment');
    
    Route::get('/notifications', [\App\Http\Controllers\NotificationController::class, 'getUserNotifications'])->name('usernotifications');
    Route::put('/notification/markasread/{notification}', [\App\Http\Controllers\NotificationController::class, 'markAsRead'])->name('markAsRead');
});
