# Unellez App

- Backend API en Laravel con autenticacion a traves de Sanctum y LDAP record.
- Aplicacion movil para Android & iOS con Xamarin.Forms

## Azure DevOps - Build Status

 | Main                                                                                                                                                                                                                                                                | Develop                                                                                                                                                                                                                                                                    |
 | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
 | [![Build Status](https://dev.azure.com/jdanieltovart/Unellez%20app/_apis/build/status/Unellez%20app?repoName=Unellez%20app&branchName=main)](https://dev.azure.com/jdanieltovart/Unellez%20app/_build/latest?definitionId=9&repoName=Unellez%20app&branchName=main) | [![Build Develop](https://dev.azure.com/jdanieltovart/Unellez%20app/_apis/build/status/Unellez%20app?repoName=Unellez%20app&branchName=develop)](https://dev.azure.com/jdanieltovart/Unellez%20app/_build/latest?definitionId=9&repoName=Unellez%20app&branchName=develop) |  |  |

## Features

- ✅❗️Inicio de sesion\
    ❗️LDAP\
    ✅ personal access token.
- ❗️Perfil\
     Datos y estatus personal.\
     Datos laborales.
- ❗️Carga familiar\
     Nombres, parentesco y estatus.
- ❗️Citas medicas\
     Detalle de la cita.
- ❗️Notificaciones\
     Creacion de citas.
     Dias antes de la fecha de la cita.

## Resources

### Mobile App

- [Autofac](https://github.com/autofac/Autofac) - an IoC container for .NET
- [AppCenter](https://docs.microsoft.com/appcenter/?WT.mc_id=mobile-0000-bramin) - continuously build, test, release, and monitor mobile apps
- [Polly](https://github.com/App-vNext/Polly) - .NET resilience and transient-fault-handling library that allows developers to express policies such as Retry, Circuit Breaker, Timeout, Bulkhead Isolation, and Fallback in a fluent and thread-safe manner
- [PancakeView](https://github.com/sthewissen/Xamarin.Forms.PancakeView) - An extended ContentView for Xamarin.Forms with rounded corners, borders, shadows and more!
- [Refit](https://github.com/reactiveui/refit) - Refit is a REST library for .NET that converts REST APIs into live interfaces
- [Sqlite-net](https://github.com/praeclarum/sqlite-net) - an open source, minimal library to allow .NET, .NET Core, and Mono applications to store data in SQLite 3 databases
- [TinyMvvm.Forms](https://github.com/TinyStuff/TinyMvvm) TinyMvvm is a MVVM library that is built for Xamarin.Forms but is not limited to Xamarin.Forms, it created in a way that it will be easy to extend to other platforms.
- [Undraw.co](https://undraw.co/) - Open-source illustrations for any idea you can imagine and create
- [Xamarin](https://docs.microsoft.com/xamarin/?WT.mc_id=mobile-0000-bramin) - build native apps for Android, iOS, and macOS using .NET code and platform-specific user interfaces
- [Xamarin.Forms](https://docs.microsoft.com/xamarin/get-started/what-is-xamarin-forms) - an open-source UI framework to build Android, iOS, and Windows applications from a single shared codebase
- [Xamarin.Essentials](https://docs.microsoft.com/xamarin/essentials) - provides cross-platform APIs for their mobile applications
- [XamarinCommunityToolkit](https://github.com/xamarin/XamarinCommunityToolkit) The Xamarin Community Toolkit is a collection of Animations, Behaviors, Converters, and Effects for mobile development with Xamarin.Forms. It simplifies and demonstrates common developer tasks building iOS, Android, and UWP apps with Xamarin.Forms.

### Backend App

- [Firebase](https://console.firebase.google.com/project/zuttv-a9b9c/overview) - Firebase is a backend as a service that is used for notifications, distribution and test the app.
- [Laravel](https://laravel.com/) - Laravel is a web application PHP framework with expressive, elegant syntax.
- [Laravel Sanctum](https://laravel.com/docs/8.x/sanctum/)Laravel Sanctum provides a featherweight authentication system for SPAs (single page applications), mobile applications, and simple, token based APIs. Sanctum allows each user of your application to generate multiple API tokens for their account. These tokens may be granted abilities / scopes which specify which actions the tokens are allowed to perform.

## Author

👤 **Daniel Tovar**

- Twitter: [@danieeis](https://twitter.com/danieeis)
- Github: [@danieeis](https://github.com/danieeis)
