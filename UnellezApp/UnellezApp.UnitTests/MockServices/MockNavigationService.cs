﻿using System;
using System.Threading.Tasks;
using UnellezApp.Shared;

namespace UnellezApp.UnitTests
{
    public class MockNavigationService : INavigationService
    {
        public Task SetLoginPage() => Task.CompletedTask;
    }
}
