﻿using System;
using Xamarin.Essentials;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace UnellezApp.UnitTests
{
    class MockDeviceInfo : IDeviceInfo
    {
        public string Model => "x86_64";

        public string Manufacturer => throw new NotImplementedException();

        public string Name => throw new NotImplementedException();

        public string VersionString => "14.4";

        public Version Version => throw new NotImplementedException();

        public DevicePlatform Platform => DevicePlatform.iOS;

        public DeviceIdiom Idiom => throw new NotImplementedException();

        public DeviceType DeviceType => throw new NotImplementedException();
    }
}
