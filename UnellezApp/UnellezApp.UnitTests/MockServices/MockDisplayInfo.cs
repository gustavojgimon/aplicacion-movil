﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace UnellezApp.UnitTests
{
    class MockDisplayInfo : DeviceInfo
    {
        public override Size PixelScreenSize => new Size(0, 0);

        public override Size ScaledScreenSize => new Size(0, 0);

        public override double ScalingFactor => 1;
    }
}
