﻿using System;
using System.Threading.Tasks;
using TinyNavigationHelper;

namespace UnellezApp.UnitTests
{
    public class MockNavigationHelper : INavigationHelper
    {
        public Task BackAsync()
        {
            return Task.CompletedTask;
        }

        public Task CloseModalAsync()
        {
            return Task.CompletedTask;
        }

        public Task NavigateToAsync(string key)
        {
            return Task.CompletedTask;
        }

        public Task NavigateToAsync(string key, object parameter)
        {
            return Task.CompletedTask;
        }

        public Task OpenModalAsync(string key, bool withNavigation = false)
        {
            return Task.CompletedTask;
        }

        public Task OpenModalAsync(string key, object parameter, bool withNavigation = false)
        {
            return Task.CompletedTask;
        }

        public Task ResetStackWith(string key)
        {
            throw new NotImplementedException();
        }

        public Task ResetStackWith(string key, object parameter)
        {
            throw new NotImplementedException();
        }

        public void SetRootView(string key, bool withNavigation = true)
        {
            throw new NotImplementedException();
        }

        public void SetRootView(string key, object parameter, bool withNavigation = true)
        {
            throw new NotImplementedException();
        }
    }
}
