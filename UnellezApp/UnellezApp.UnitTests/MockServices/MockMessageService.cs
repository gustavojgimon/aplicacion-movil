﻿using System;
using System.Threading.Tasks;
using UnellezApp.Shared;

namespace UnellezApp.UnitTests
{
    public class MockMessageService : IMessageService
    {
        public Task DisplayAlert(string title, string message, string buttonText)
        {
            System.Diagnostics.Debug.WriteLine(message);

            return Task.CompletedTask;
        }

        public void Error(params string[] message)
        {
            System.Diagnostics.Debug.WriteLine(string.Join(", ", message));
        }

        public void HideLoading()
        {
            System.Diagnostics.Debug.WriteLine("Finish loading");
        }

        public void ShowLoading(string loadingMessage)
        {
            System.Diagnostics.Debug.WriteLine(loadingMessage);
        }

        public void Toast(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }
    }
}
