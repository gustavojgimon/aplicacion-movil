﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using RichardSzalay.MockHttp;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace UnellezApp.UnitTests
{
    abstract class BaseTest
    {
        [TearDown]
        public virtual Task TearDown() => Task.CompletedTask;

        [SetUp]
        public virtual async Task Setup()
        {
            InitializeServiceCollection();

            //FFImageLoading.ImageService.EnableMockImageService = true;

            CultureInfo.DefaultThreadCurrentCulture = null;
            CultureInfo.DefaultThreadCurrentUICulture = null;

            Device.Info = new MockDisplayInfo();
            Device.PlatformServices = new MockPlatformServices();

            var preferences = ServiceCollection.ServiceProvider.GetRequiredService<IPreferences>();
            preferences.Clear();

            var secureStorage = ServiceCollection.ServiceProvider.GetRequiredService<ISecureStorage>();
            secureStorage.RemoveAll();

            var authService = ServiceCollection.ServiceProvider.GetRequiredService<AuthenticationService>();
            await authService.ForceLogout(CancellationToken.None);

            //var notificationService = ServiceCollection.ServiceProvider.GetRequiredService<NotificationService>();
            //await notificationService.SetAppBadgeCount(0).ConfigureAwait(false);
            //notificationService.UnRegister();

            //var mockNotificationService = (MockDeviceNotificationsService)ServiceCollection.ServiceProvider.GetRequiredService<IDeviceNotificationsService>();
            //mockNotificationService.Reset();
        }

        protected virtual void InitializeServiceCollection()
        {
            var userApiClient = RefitExtensions.For<IUserApi>(CreateHttpClient(Helpers.Secrets.ApiBackend));
           
            ServiceCollection.Initialize(userApiClient);
        }

        protected static async Task AuthenticateUser(UserService userService)
        {
            var token = new UnellezToken("71|ZP6pqvWosBhbNFCvZ6IIpkdtektiXmc2Fi5p6Tt7", "Bearer");
            if (token.IsEmpty() || string.IsNullOrWhiteSpace(token.AccessToken))
                throw new Exception("Invalid Token");

            await userService.SaveToken(token).ConfigureAwait(false);
            var _unellezApiService = ServiceCollection.ServiceProvider.GetRequiredService<UnellezApiService>();

            //var userInfo = await _unellezApiService.GetUserInfo(token, CancellationToken.None);

            userService.User = new UserInfo(string.Empty, string.Empty, "Jose Daniel", "Tovar Torrealba", "Activo", string.Empty, "Nomina", "Cargo", "Vicereptorado", "Departamento");
        }

        static HttpClient CreateHttpClient(string url)
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            httpMessageHandler.When($"{url}/api/v1/logout").Respond("application/json","1");

            httpMessageHandler.When($"{url}/api/v1/login").Respond("application/json", "{" +
                                                            "'access_token': '71|ZP6pqvWosBhbNFCvZ6IIpkdtektiXmc2Fi5p6Tt7'," +
                                                            "'token_type': 'Bearer'" +
                                                            "}");
            httpMessageHandler.When($"{url}/api/v1/user").Respond("application/json", "{'Codigo':'6983','Cedula':'19430853','Nombres':'GUSTAVO JOS\u00c9','Apellidos':'GIMON VILLENA','Estatus':'ACTIVO','Correo':'gustavojgimon@gmail.com','Nomina':'ADMINISTRATIVO ACTIVO','Cargo':'ADMINISTRADOR DE TECNOLOGIA DE INFORMACION Y COMUN','Vicerectorado':'CENTRAL','Departamento':'COORDINACI\u00d3N DE TECNOLOG\u00cdA DE INFORMACI\u00d3N CENTRAL'}");
            httpMessageHandler.When($"{url}/sanctum/csrf-cookie").Respond(response => new HttpResponseMessage(HttpStatusCode.OK));
            var httpClient = httpMessageHandler.ToHttpClient();
            httpClient.BaseAddress = new Uri(url);

            return httpClient;
        }
    }
}