﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Refit;
using RichardSzalay.MockHttp;
using UnellezApp.Mobile.Common;
using UnellezApp.Services;
using UnellezApp.Shared;
using UnellezApp.ViewModels;

namespace UnellezApp.UnitTests.ViewModels
{
    class DashboardViewModelUnauthorizedTest : BaseTest
    {
        [Test]
        public async Task ShouldLogoutWhenPullToRefresh()
        {
            //Arrange
            string emptyDataViewTitle_Initial, emptyDataViewTitle_Final;

            bool didPullToRefreshFailedFire = false;
            var pullToRefreshFailedTCS = new TaskCompletionSource<PullToRefreshFailedEventArgs>();

            DashboardViewModel.PullToRefreshFailed += HandlePullToRefreshFailed;

            var dashboardViewModel = ServiceCollection.ServiceProvider.GetRequiredService<DashboardViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();
            await AuthenticateUser(userService).ConfigureAwait(false);

            //Act
            emptyDataViewTitle_Initial = dashboardViewModel.Name;

            var refreshCommandTask = dashboardViewModel.RefreshCommand.ExecuteAsync();

            await refreshCommandTask.ConfigureAwait(false);
            var pullToRefreshFailedEventArgs = await pullToRefreshFailedTCS.Task.ConfigureAwait(false);

            emptyDataViewTitle_Final = dashboardViewModel.Name;

            //Assert
            Assert.IsTrue(didPullToRefreshFailedFire);
            Assert.IsTrue(pullToRefreshFailedEventArgs is LoginExpiredPullToRefreshEventArgs);
            Assert.IsNotEmpty(emptyDataViewTitle_Initial);
            Assert.IsEmpty(emptyDataViewTitle_Final);

            void HandlePullToRefreshFailed(object? sender, PullToRefreshFailedEventArgs e)
            {
                DashboardViewModel.PullToRefreshFailed -= HandlePullToRefreshFailed;

                didPullToRefreshFailedFire = true;
                pullToRefreshFailedTCS.SetResult(e);
            }
        }

        protected override void InitializeServiceCollection()
        {
            var userApiClient = RefitExtensions.For<IUserApi>(CreateLoginFailedHttpClient(Helpers.Secrets.ApiBackend));

            ServiceCollection.Initialize(userApiClient);
        }

        static HttpClient CreateLoginFailedHttpClient(string url)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized);

            var httpMessageHandler = new MockHttpMessageHandler();
            httpMessageHandler.When($"{url}/api/v1/user").Respond(request => responseMessage);

            var httpClient = httpMessageHandler.ToHttpClient();
            httpClient.BaseAddress = new Uri(url);

            return httpClient;
        }
    }
}
