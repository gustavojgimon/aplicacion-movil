﻿using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using UnellezApp.Services;
using UnellezApp.ViewModels;
using Xamarin.CommunityToolkit.ObjectModel;

namespace UnellezApp.UnitTests.ViewModels
{
    [TestFixture]
    class DashboardViewModelTest : BaseTest
    {
        [Test]
        public async Task ShouldInit()
        {
            var dashboardViewModel = ServiceCollection.ServiceProvider.GetRequiredService<DashboardViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();

            await AuthenticateUser(userService).ConfigureAwait(false);

            dashboardViewModel.Name.Should().NotBeEmpty();
            dashboardViewModel.LogoutCommand.Should().NotBeNull().And.BeOfType<AsyncCommand>();
            dashboardViewModel.RefreshCommand.Should().NotBeNull().And.BeOfType<AsyncCommand>();
        }

        [TestCase("Jose Daniel")]
        [TestCase("Jose")]
        [TestCase("Jose D.")]
        public async Task ShouldDisplayNameCorrectly(string name)
        {
            //Arrange
            var dashboardViewModel = ServiceCollection.ServiceProvider.GetRequiredService<DashboardViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();
            //Act
            await AuthenticateUser(userService).ConfigureAwait(false);
            userService.User = userService.User! with { Nombres = name };
            //Assert
            dashboardViewModel.Name.Should().NotBeEmpty();
            dashboardViewModel.Name.Should().Be("Jose");
        }

        [Test]
        public async Task ShouldLogout()
        {
            var dashboardViewModel = ServiceCollection.ServiceProvider.GetRequiredService<DashboardViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();
            await AuthenticateUser(userService).ConfigureAwait(false);

            await dashboardViewModel.LogoutCommand.ExecuteAsync().ConfigureAwait(false);

            dashboardViewModel.Name.Should().BeEmpty();
            userService.IsAuthenticated.Should().BeFalse();
        }

        [Test]
        public async Task ShouldPullToRefresh_AuthenticatedUser()
        {
            //Arrange
            string emptyDataViewTitle_Initial, emptyDataViewTitle_Final;

            var dashboardViewModel = ServiceCollection.ServiceProvider.GetRequiredService<DashboardViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();


            emptyDataViewTitle_Initial = dashboardViewModel.Name;
            //Act
            await AuthenticateUser(userService).ConfigureAwait(false);

            var refreshCommandTask = dashboardViewModel.RefreshCommand.ExecuteAsync();

            await refreshCommandTask.ConfigureAwait(false);

            emptyDataViewTitle_Final = dashboardViewModel.Name;

            //Asset
            emptyDataViewTitle_Initial.Should().BeEmpty();
            emptyDataViewTitle_Final.Should().Contain(userService.Name);
        }
    }
}
