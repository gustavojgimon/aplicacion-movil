﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using UnellezApp.Services;
using UnellezApp.ViewModels;

namespace UnellezApp.UnitTests.ViewModels
{
    [TestFixture]
    class ProfileViewModelTest : BaseTest
    {
        [TestCase("Daniel", "Tovar", Description = "Password empty")]
        [TestCase("Daniel", "Tovar Torrealba", Description = "Email empty")]
        [TestCase("Daniel B.", "Tovar Torrealba", Description = "Email empty")]
        public async Task ShouldDisplayName(string name, string lastName)
        {
            //Arrange
            var profileViewModel = ServiceCollection.ServiceProvider.GetRequiredService<ProfileViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();

            //Act
            await AuthenticateUser(userService).ConfigureAwait(false);
            userService.User = userService.User! with { Nombres = name, Apellidos = lastName };
            //Assert
            profileViewModel.AvatarImage.Should().NotBeNull();
            profileViewModel.Initials.Should().NotBeNull();
            profileViewModel.ProfileName.Should().NotBeNull();
            profileViewModel.LocationWork.Should().NotBeNull();
            profileViewModel.OrganizationWorkStatus.Should().NotBeNull();
            profileViewModel.StatusName.Should().NotBeNull();
            profileViewModel.PersonalInfoList.Should().NotBeNull();
            profileViewModel.ProfileName.Should().NotBeEmpty();
            profileViewModel.ProfileName.Should().Be("Daniel Tovar");
            profileViewModel.Initials.Should().Be("DT");
            profileViewModel.PersonalInfoList.Should().HaveCountGreaterOrEqualTo(2);
        }
    }
}
