﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using RichardSzalay.MockHttp;
using UnellezApp.Helpers;
using UnellezApp.Mobile.Common.Constants;
using UnellezApp.Services;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.UnitTests.ViewModels
{
    [TestFixture]
    class LoginViewModelTest_LoginFailed : BaseTest
    {
        [TestCase("danieltovar@unellez.com", "", Description = "Password empty")]
        [TestCase("danieltovarunellez.com", "", Description = "Email invalid")]
        [TestCase("danieltovarunellezcom", "", Description = "Email invalid")]
        [TestCase("danie@ltovarunellezcom", "", Description = "Email invalid")]
        [TestCase("danie@ sad.com", "", Description = "Pass empty")]
        [TestCase("", "password", Description = "Email empty")]
        public async Task FailLoginRequest(string emailTestCase, string passwordTestCase)
        {
            //arrange
            var loginViewModel = ServiceCollection.ServiceProvider.GetRequiredService<LoginViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();
            var preferences = ServiceCollection.ServiceProvider.GetRequiredService<IPreferences>();
            loginViewModel.Email = emailTestCase;
            loginViewModel.Password = passwordTestCase;

            //act
            await loginViewModel.LoginCommand.ExecuteAsync().ConfigureAwait(false);

            //assert
            preferences.ContainsKey(Secrets.MostRecentSessionId).Should().BeFalse();
            preferences.ContainsKey(Secrets.OAuthToken).Should().BeFalse();

            if (string.IsNullOrEmpty(emailTestCase))
            {
                loginViewModel.ErrorText.Should().Be(LoginPageConstants.MustEnterEmail);
            }
            else if (string.IsNullOrEmpty(passwordTestCase))
            {
                loginViewModel.ErrorText.Should().Be(LoginPageConstants.MustEnterPassword);
            }
            else
            {
                loginViewModel.ErrorText.Should().Be(LoginPageConstants.EmailMustBeValid);
            }
            loginViewModel.ErrorText.Should().NotBeNullOrWhiteSpace();
            userService.IsAuthenticated.Should().BeFalse();

        }

        protected override void InitializeServiceCollection()
        {
            var userApiClient = RefitExtensions.For<IUserApi>(CreateLoginFailedHttpClient(Secrets.ApiBackend));

            ServiceCollection.Initialize(userApiClient);
        }

        static HttpClient CreateLoginFailedHttpClient(string url)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.UnprocessableEntity);

            var httpMessageHandler = new MockHttpMessageHandler();
            httpMessageHandler.When($"{url}/*").Respond(request => responseMessage);

            var httpClient = httpMessageHandler.ToHttpClient();
            httpClient.BaseAddress = new Uri(url);

            return httpClient;
        }
    }
}
