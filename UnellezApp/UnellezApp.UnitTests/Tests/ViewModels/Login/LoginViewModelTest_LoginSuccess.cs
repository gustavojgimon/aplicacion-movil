using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using RichardSzalay.MockHttp;
using UnellezApp.Helpers;
using UnellezApp.Mobile.Common.Constants;
using UnellezApp.Services;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.UnitTests.ViewModels
{
    [TestFixture]
    class LoginViewModelTest_LoginSuccess : BaseTest
    {
        [Test]
        public async Task SuccessLoginRequest()
        {
            //arrange
            var loginViewModel = ServiceCollection.ServiceProvider.GetRequiredService<LoginViewModel>();
            var userService = ServiceCollection.ServiceProvider.GetRequiredService<UserService>();
            var preferences = ServiceCollection.ServiceProvider.GetRequiredService<IPreferences>();
            loginViewModel.Email = "newton@ldap.forumsys.com";
            loginViewModel.Password = "password";
            loginViewModel.EmailValid = true;
            //act
            await loginViewModel.LoginCommand.ExecuteAsync().ConfigureAwait(false);
            //assert
            preferences.ContainsKey(Secrets.MostRecentSessionId).Should().BeTrue();
            preferences.ContainsKey(Secrets.OAuthToken).Should().BeTrue();
            preferences.ContainsKey(Secrets.IsAuthenticated).Should().BeTrue();
            loginViewModel.Email.Should().BeEmpty();
            loginViewModel.Password.Should().BeEmpty();
            userService.IsAuthenticated.Should().BeTrue();
            var token = await userService.GetAuthToken();
            token.Should().NotBe(UnellezToken.Empty);
            userService.Name.Should().NotBeEmpty();
        }
    }
}