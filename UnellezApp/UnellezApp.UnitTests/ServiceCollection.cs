﻿using System;
using Microsoft.Extensions.DependencyInjection;
using TinyNavigationHelper;
using UnellezApp.Services;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.UnitTests
{
    static class ServiceCollection
    {
        static IServiceProvider? _serviceProviderHolder;

        public static IServiceProvider ServiceProvider => _serviceProviderHolder ?? throw new NullReferenceException("Must call Initialize first");

        public static void Initialize(IUserApi userApiClient) =>
            _serviceProviderHolder = CreateContainer(userApiClient);

        static IServiceProvider CreateContainer(IUserApi userApiClient)
        {
            var services = new Microsoft.Extensions.DependencyInjection.ServiceCollection();

            //Unellez Refit Services
            services.AddSingleton(userApiClient);

            //UnellezApp Services
            services.AddSingleton<UserService>();
            services.AddSingleton<AuthenticationService>();
            services.AddSingleton<UnellezApiService>();
            services.AddSingleton<AppInitializationService>();
            services.AddSingleton<LanguageService>();
            services.AddSingleton<ThemeService>();

            //UnellezApp ViewModels
            services.AddTransient<LoginViewModel>();
            services.AddTransient<DashboardViewModel>();
            services.AddTransient<ProfileViewModel>();
            services.AddTransient<FamilyBurdenViewModel>();
            services.AddTransient<MedicalAppointmentDetailViewModel>();
            services.AddTransient<MedicalAppointmentsViewModel>();
            services.AddTransient<NotificationViewModel>();

            //Mocks
            services.AddSingleton<IDeviceInfo, MockDeviceInfo>();
            services.AddSingleton<ISecureStorage, MockSecureStorage>();
            services.AddSingleton<IPreferences, MockPreferences>();
            services.AddSingleton<IMessageService, MockMessageService>();
            services.AddSingleton<INavigationHelper, MockNavigationHelper>();
            services.AddSingleton<IConnectivity, MockConnectivity>();
            services.AddSingleton<IAnalyticsService, MockAnalyticsService>();
            services.AddSingleton<IMainThread, MockMainThread>();
            services.AddSingleton<INavigationService, MockNavigationService>();

            return services.BuildServiceProvider();
        }
    }
}
