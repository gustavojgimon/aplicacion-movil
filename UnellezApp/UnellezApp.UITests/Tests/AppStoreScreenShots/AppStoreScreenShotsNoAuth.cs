﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;

namespace UnellezApp.UITests
{
    [TestFixture(Platform.Android, UserType.Neither)]
    [TestFixture(Platform.iOS, UserType.Neither)]
    class AppStoreScreenShotsNoAuth : BaseUITest
    {
        public AppStoreScreenShotsNoAuth(Platform platform, UserType userType) : base(platform, userType)
        {
        }

        [Test]
        public async Task AppStoreScreenShotsTest()
        {
            //Arrange
            var screenRect = App.Query().First().Rect;

            //Act
            await LoginPage.WaitForViewToLoad().ConfigureAwait(false);
            App.Screenshot("Login Page Light");

            // Change theme and do it again

            //Assert
        }
    }
}
