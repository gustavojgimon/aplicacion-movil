﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;

namespace UnellezApp.UITests
{
    [TestFixture(Platform.Android, UserType.LoggedIn)]
    [TestFixture(Platform.iOS, UserType.LoggedIn)]
    class AppStoreScreenShotsAuth : BaseUITest
    {
        public AppStoreScreenShotsAuth(Platform platform, UserType userType) : base(platform, userType)
        {
        }

        [Test]
        public async Task AppStoreScreenShotsTest()
        {
            //Arrange
            var screenRect = App.Query().First().Rect;

            //Act
            App.Screenshot("Dashboard Page Light");

            DashboardPage.TapProfileCard();

            //await ProfilePage.WaitForPageToLoad().ConfigureAwait(false);

            //App.TouchAndHoldCoordinates(screenRect.CenterX, screenRect.CenterY);
            //App.Screenshot("Profile Page Light");

            //ProfilePage.TapBackButton();
            //await DashboardPage.WaitForViewToLoad().ConfigureAwait(false);
            DashboardPage.TapFamilyCard();
            //await FamilyPage.WaitForPageToLoad().ConfigureAwait(false);

            //App.Screenshot("Family Page Light");

            //FamilyPage.TapBackButton();
            //await DashboardPage.WaitForViewToLoad().ConfigureAwait(false);

            DashboardPage.TapMedicalCard();
            //await MedicalPage.WaitForPageToLoad().ConfigureAwait(false);

            //App.Screenshot("Medical Page Light");

            //MedicalPage.TapBackButton();
            //await DashboardPage.WaitForViewToLoad().ConfigureAwait(false);

            DashboardPage.TapNotificationsCard();
            //await NotificationsPage.WaitForPageToLoad().ConfigureAwait(false);

            //App.Screenshot("Medical Page Light");

            //NotificationsPage.TapBackButton();
            //await DashboardPage.WaitForViewToLoad().ConfigureAwait(false);

            /// Change theme and do it again

            //Assert
        }
    }
}
