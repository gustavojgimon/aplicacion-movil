﻿using System;
using NUnit.Framework;
using Xamarin.UITest;

namespace UnellezApp.UITests
{
    [TestFixture(Platform.Android, UserType.Neither)]
    [TestFixture(Platform.iOS, UserType.Neither)]
    class LaunchTests : BaseUITest
    {
        public LaunchTests(Platform platform, UserType userType) : base(platform, userType)
        {
        }

        [Test]
        public void LaunchTest()
        {
            try
            {
                SplashScreenPage.WaitForViewToLoad(TimeSpan.FromSeconds(1));
            }
            catch
            {
                LoginPage.WaitForViewToLoad(TimeSpan.FromSeconds(10));
            }
        }
    }
}
