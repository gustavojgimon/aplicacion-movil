﻿using System;
using NUnit.Framework;
using UnellezApp.UITests.Views;
using Xamarin.UITest;
using UnellezApp.Mobile.Common;
using System.Threading.Tasks;
using UnellezApp.Shared;

namespace UnellezApp.UITests
{
    enum UserType { Demo, LoggedIn, Neither }

    abstract class BaseUITest
    {
        readonly Platform _platform;

        IApp? _app;
        LoginPage? _loginPage;
        DashboardPage? _dashboardPage;
        SplashScreenPage? _splashScreenPage;

        protected BaseUITest(Platform platform, UserType userType)
        {
            _platform = platform;
            UserType = userType;
        }

        protected UserType UserType { get; }

        protected IApp App => _app ?? throw new NullReferenceException();
        protected LoginPage LoginPage => _loginPage ?? throw new NullReferenceException();
        protected DashboardPage DashboardPage => _dashboardPage ?? throw new NullReferenceException();
        protected SplashScreenPage SplashScreenPage => _splashScreenPage ?? throw new NullReferenceException();

        [SetUp]
        public virtual Task BeforeEachTest()
        {
            _app = AppInitializer.StartApp(_platform);

            _loginPage = new LoginPage(App);
            _dashboardPage = new DashboardPage(App);
            _splashScreenPage = new SplashScreenPage(App);

            App.Screenshot("App initialized");

            return UserType switch
            {
                UserType.Neither => SetupNeither(),
                UserType.LoggedIn => SetupLoggedInUser(),
                _ => throw new NotSupportedException()
            };
        }

        protected Task SetupNeither() => LoginPage.WaitForViewToLoad();

        protected async Task SetupLoggedInUser()
        {
            await LoginPage.WaitForViewToLoad().ConfigureAwait(false);

            await LoginToUnellez().ConfigureAwait(false);

            LoginPage.ClosePage();

            await DashboardPage.WaitForViewToLoad().ConfigureAwait(false);
        }

        protected async Task LoginToUnellez()
        {
            var token = await UserApiService.GetTestToken();

            UnellezToken? currentUserToken = null;

            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);

            while (currentUserToken is null)
            {
                App.InvokeBackdoorMethod(BackdoorMethodConstants.SetUnellezUser, token.AccessToken);

                await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);

                currentUserToken = App.InvokeBackdoorMethod<UnellezToken?>(BackdoorMethodConstants.GetAuthToken);
            }

            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);

            App.Screenshot("Logged In");
        }
    }
}
