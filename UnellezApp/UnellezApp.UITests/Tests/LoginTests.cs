﻿using System.Threading.Tasks;
using NUnit.Framework;
using UnellezApp.Mobile.Common.Constants;
using Xamarin.UITest;

namespace UnellezApp.UITests
{
    [TestFixture(Platform.Android, UserType.Neither)]
    [TestFixture(Platform.iOS, UserType.Neither)]
    class LoginTest : BaseUITest
    {
        public LoginTest(Platform platform, UserType userType) : base(platform, userType)
        {

        }

        [SetUp]
        public override async Task BeforeEachTest()
        {
            await base.BeforeEachTest().ConfigureAwait(false);

            await LoginPage.WaitForViewToLoad().ConfigureAwait(false);
        }

        [Test]
        public async void CompleteSuccessFormLogin()
        {
            //Arrange
            string email = "newton@ldap.forumsys.com";
            string pass = "password";
            //Arrange & Act
            LoginPage.EnterCredentials(email, pass);
            LoginPage.TapLoginButton();
            //Asserts
            LoginPage.ShouldLoad();
            Assert.IsFalse(LoginPage.ErrorIsVisible);
            await DashboardPage.WaitForViewToLoad();
        }

        [Test]
        public void CompleteBadLogin()
        {
            //Arrange
            string email = "marilyne.bd";
            string pass = "awDAS24ras";
            //Arrange & Act
            LoginPage.EnterCredentials(email, pass);
            LoginPage.TapLoginButton();
            //Asserts
            LoginPage.ShouldLoadWithErrors();
        }

        [Test]
        public void MissingEmailDisplaysWarning()
        {
            LoginPage.EnterCredentials(string.Empty, "password");
            LoginPage.TapLoginButton();

            LoginPage.ShouldLoadWithErrors(LoginPageConstants.MustEnterEmail);
        }

        [Test]
        public void MissingPasswordDisplaysWarning()
        {
            LoginPage.EnterCredentials("danieltovar@unellez.com", string.Empty);
            LoginPage.TapLoginButton();

            LoginPage.ShouldLoadWithErrors(LoginPageConstants.MustEnterPassword);
        }
    }
}
