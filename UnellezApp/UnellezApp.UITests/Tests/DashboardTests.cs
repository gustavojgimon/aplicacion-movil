﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;

namespace UnellezApp.UITests
{
    [TestFixture(Platform.iOS, UserType.LoggedIn)]
    [TestFixture(Platform.Android, UserType.LoggedIn)]
    class DashboardTests : BaseUITest
    {
        public DashboardTests(Platform platform, UserType userType) : base(platform, userType)
        {
        }

        public override async Task BeforeEachTest()
        {
            await base.BeforeEachTest();

            await DashboardPage.WaitForViewToLoad().ConfigureAwait(false);
        }

        [Test]
        public void EnsureUnellezCardOpensBrowser()
        {
            //Arrange

            //Act
            DashboardPage.TapLogoUnellezCard();

            //Assert
            if (App is iOSApp)
            {
                DashboardPage.WaitForBrowserToOpen();
                Assert.IsTrue(DashboardPage.IsBrowserOpen);
            }
        }

        [Test]
        public void VerifyLogOutButton()
        {
            //Arrange

            //Act
            DashboardPage.TapLogoutButton();

            //Assert
            LoginPage.WaitForViewToLoad();
        }
    }
}
