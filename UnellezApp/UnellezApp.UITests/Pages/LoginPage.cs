﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using UnellezApp.Mobile.Common;
using UnellezApp.Mobile.Common.Constants;
using Xamarin.UITest;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;
namespace UnellezApp.UITests
{
    class LoginPage : BasePage
    {
        readonly Query emailEntry, passwordEntry, errorLabel, loginButton, activityIndicator, unellezImage,
            titleForm, emailLabel, passwordLabel;

        public LoginPage(IApp app) : base(app, () => PageTitles.LoginPage)
        {
            emailEntry = GenerateMarkedQuery(LoginPageAutomationIds.EmailEntry);
            passwordEntry = GenerateMarkedQuery(LoginPageAutomationIds.PasswordEntry);
            loginButton = GenerateMarkedQuery(LoginPageAutomationIds.SubmitButton);
            errorLabel = GenerateMarkedQuery(LoginPageAutomationIds.ErrorLabel);
            activityIndicator = GenerateMarkedQuery(LoginPageAutomationIds.ActivityIndicator);
            unellezImage = GenerateMarkedQuery(LoginPageAutomationIds.UnellezImage);
            titleForm = GenerateMarkedQuery(LoginPageAutomationIds.TitleForm);
            emailLabel = GenerateMarkedQuery(LoginPageAutomationIds.EmailLabel);
            passwordLabel = GenerateMarkedQuery(LoginPageAutomationIds.PasswordLabel);
        }

        public override Task WaitForViewToLoad(TimeSpan? timespan = null)
        {
            Assert.AreEqual(LoginPageConstants.LoginButton, GetText(loginButton));
            Assert.AreEqual(LoginPageConstants.LoginTitle, GetText(titleForm));
            Assert.AreEqual(LoginPageConstants.EmailLabel, GetText(emailLabel));
            Assert.AreEqual(LoginPageConstants.PasswordLabel, GetText(passwordLabel));
            App.WaitForElement(unellezImage);
            return Task.CompletedTask;
        }

        public void EnterCredentials(string email, string pass)
        {
            EnterText(emailEntry, email);

            EnterText(passwordEntry, pass);
            App.Screenshot("Credentials entered");
        }

        public void TapLoginButton()
        {
            App.Tap(loginButton);
            App.Screenshot("Login button tapped");
        }

        public bool ErrorIsVisible
        {
            get
            {
                return App.Query(errorLabel).Length > 0;
            }
        }

        internal void ShouldLoadWithErrors(string? text = null)
        {
            ShouldLoad();
            Assert.IsTrue(ErrorIsVisible);
            if (text != null)
                ErrorDisplaysCorrectText(text);
            App.Screenshot("Error appeared");

            App.WaitForNoElement(errorLabel);
            App.Screenshot("Error leaved");
        }

        internal void ShouldLoad()
        {
            App.WaitForNoElement(activityIndicator);
        }

        public bool ErrorDisplaysCorrectText(string text)
            => App.Query(errorLabel)
                    .FirstOrDefault().Text == text;

        public void ClosePage()
        {
            App.InvokeBackdoorMethod(BackdoorMethodConstants.CloseLoginPage);
            App.Screenshot("Login View Popped");
        }
    }
}
