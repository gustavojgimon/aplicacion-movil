﻿using System;
using System.Linq;
using System.Threading.Tasks;
using UnellezApp.Mobile.Common;
using UnellezApp.Mobile.Common.Constants;
using Xamarin.UITest;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace UnellezApp.UITests.Views
{
    class DashboardPage : BasePage
    {
        readonly Query userCardTitle, userCardDescription, userCardIcon, profileCard,
            familyCard, medicalCard, unellezLogoCard, notificationsCard, logoutToolbar,
            profileCardTitle, familyCardTitle, medicalCardTitle, notificationsCardTitle;

        public DashboardPage(IApp app) : base(app, () => PageTitles.DashboardPage)
        {
            userCardTitle = GenerateMarkedQuery(DashboardPageAutomationIds.UserCardTitle);
            userCardDescription = GenerateMarkedQuery(DashboardPageAutomationIds.UserCardDescription);
            userCardIcon = GenerateMarkedQuery(DashboardPageAutomationIds.UserCardIcon);
            profileCard = GenerateMarkedQuery(DashboardPageAutomationIds.ProfileCard);
            familyCard = GenerateMarkedQuery(DashboardPageAutomationIds.FamilyCard);
            medicalCard = GenerateMarkedQuery(DashboardPageAutomationIds.MedicalCard);
            unellezLogoCard = GenerateMarkedQuery(DashboardPageAutomationIds.UnellezCard);
            notificationsCard = GenerateMarkedQuery(DashboardPageAutomationIds.NotificationsCard);
            logoutToolbar = GenerateMarkedQuery(DashboardPageAutomationIds.LogoutToolbar);
            profileCardTitle = GenerateMarkedQuery(DashboardPageAutomationIds.ProfileCardTitle);
            familyCardTitle = GenerateMarkedQuery(DashboardPageAutomationIds.FamilyCardTitle);
            medicalCardTitle = GenerateMarkedQuery(DashboardPageAutomationIds.MedicalCardTitle);
            notificationsCardTitle = GenerateMarkedQuery(DashboardPageAutomationIds.NotificationsCardTitle);
        }

        public override async Task WaitForViewToLoad(TimeSpan? timeout = null)
        {
            await base.WaitForViewToLoad(timeout).ConfigureAwait(false);

            App.WaitForElement(userCardTitle);
            App.WaitForElement(userCardDescription);
            App.WaitForElement(userCardIcon);
            App.WaitForElement(profileCard);
            App.WaitForElement(familyCard);
            App.WaitForElement(medicalCard);
            App.WaitForElement(unellezLogoCard);
            App.WaitForElement(profileCardTitle);
            App.WaitForElement(logoutToolbar);
            App.WaitForElement(familyCardTitle);
            App.WaitForElement(medicalCardTitle);
            ScrollTo(notificationsCard);
            App.WaitForElement(notificationsCard);
            App.WaitForElement(notificationsCardTitle);
        }

        public void TapLogoutButton()
        {
            App.Tap(logoutToolbar);
            App.Screenshot("Logout button tapped");
        }

        public void TapProfileCard()
        {
            App.Tap(logoutToolbar);
            App.Screenshot("Logout button tapped");
        }

        public void TapFamilyCard()
        {
            App.Tap(logoutToolbar);
            App.Screenshot("Logout button tapped");
        }

        public void TapMedicalCard()
        {
            App.Tap(logoutToolbar);
            App.Screenshot("Logout button tapped");
        }

        public void TapLogoUnellezCard()
        {
            App.Tap(logoutToolbar);
            App.Screenshot("Logout button tapped");
        }

        public void TapNotificationsCard()
        {
            App.Tap(logoutToolbar);
            App.Screenshot("Logout button tapped");
        }
    }
}
