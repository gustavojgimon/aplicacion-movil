﻿using System;
using System.Threading.Tasks;
using Xamarin.UITest;
using UnellezApp.Mobile.Common;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace UnellezApp.UITests.Views
{
    class SplashScreenPage : BasePage
    {
        readonly Query _unellezImage, _statusLabel;

        public SplashScreenPage(IApp app) : base(app)
        {
            _unellezImage = GenerateMarkedQuery(SplashScreenPageAutomationIds.UnellezImage);
            _statusLabel = GenerateMarkedQuery(SplashScreenPageAutomationIds.StatusLabel);
        }

        public string StatusLabelText => GetText(_statusLabel);

        public override Task WaitForViewToLoad(TimeSpan? timespan = null)
        {
            App.WaitForElement(_unellezImage);
            return Task.CompletedTask;
        }
    }
}
