﻿using System;
namespace UnellezApp
{
    public class InitializationCompleteEventArgs : EventArgs
    {
        public InitializationCompleteEventArgs(bool isInitializationSuccessful) =>
            IsInitializationSuccessful = isInitializationSuccessful;

        public bool IsInitializationSuccessful { get; }
    }
}
