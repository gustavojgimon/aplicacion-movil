﻿using System;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using TinyNavigationHelper.Abstraction;
using UnellezApp.Pages;

namespace UnellezApp.ViewModels
{
    public class MedicalAppointmentsViewModel : MainViewModel
    {
        MedicalAppointmentDTO? _appointmentSelected;
        public MedicalAppointmentDTO? AppointmentSelected
        {
            get => _appointmentSelected;
            set => Set(ref _appointmentSelected, value);
        }

        public ICommand OpenDetail
        {
            get
            {
                return new Command(() =>
                {
                    if (AppointmentSelected is null) return;
                    NavigationHelper.Current.NavigateToAsync(nameof(MedicalAppointmentDetailPage), AppointmentSelected.Id);
                    AppointmentSelected = null;
                });
            }
        }

        readonly UnellezApiService _unellezApiService;
        readonly UserService _userService;

        IReadOnlyList<MedicalAppointmentDTO> _medicalAppoinmentsList = Array.Empty<MedicalAppointmentDTO>();


        public MedicalAppointmentsViewModel(IMainThread mainThread,
                                IMessageService messageService,
                                IAnalyticsService analyticsService,
                                AuthenticationService authenticationService,
                                UserService userService,
                                UnellezApiService unellezApiService) : base(mainThread, messageService, analyticsService, authenticationService)
        {
            _userService = userService;
            _unellezApiService = unellezApiService;
        }

        public IReadOnlyList<MedicalAppointmentDTO> MedicalAppoinmentsList
        {
            get => _medicalAppoinmentsList;
            set => Set(ref _medicalAppoinmentsList, value);
        }

        public async override Task Refresh()
        {
            var token = await _userService.GetAuthToken().ConfigureAwait(false);
            var list = await _unellezApiService.GetMedicalAppointments(token, CancellationToken.None).ConfigureAwait(false);
            MedicalAppoinmentsList = new ObservableCollection<MedicalAppointmentDTO>(list.Select(input => new MedicalAppointmentDTO(input.Id, input.Specialization, input.DoctorName, input.Date, input.Location)));
        }
    }
}
