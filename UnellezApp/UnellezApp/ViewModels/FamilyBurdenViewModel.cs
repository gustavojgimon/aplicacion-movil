﻿using System;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace UnellezApp.ViewModels
{
    public class FamilyBurdenViewModel : MainViewModel
    {
        readonly UnellezApiService _unellezApiService;
        readonly UserService _userService;

        IReadOnlyList<FamilyBurdenDTO> _familyBurdenList = Array.Empty<FamilyBurdenDTO>();

        public FamilyBurdenViewModel(IMainThread mainThread,
                                IMessageService messageService,
                                IAnalyticsService analyticsService,
                                AuthenticationService authenticationService,
                                UserService userService,
                                UnellezApiService unellezApiService) : base(mainThread, messageService, analyticsService, authenticationService)
        {
            _userService = userService;
            _unellezApiService = unellezApiService;
        }

        public IReadOnlyList<FamilyBurdenDTO> FamilyBurdenList
        {
            get => _familyBurdenList;
            set => Set(ref _familyBurdenList, value);
        }

        public async override Task Refresh()
        {
            var token = await _userService.GetAuthToken().ConfigureAwait(false);
            var list = await _unellezApiService.GetFamilyBurder(token, CancellationToken.None);
            FamilyBurdenList = new ObservableCollection<FamilyBurdenDTO>(list.Select(input => new FamilyBurdenDTO(input.Names.ToPascalCase(), input.DNI, input.Gender, input.BirthDate, input.Relationship)));
        }
    }
}
