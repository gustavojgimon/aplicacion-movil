﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnellezApp.Mobile.Common;
using UnellezApp.Mobile.Common.Constants;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.CommunityToolkit.ObjectModel;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.ViewModels
{
    public class DashboardViewModel : MainViewModel
    {
        readonly static DelegateWeakEventManager _notificationChangedEventManager = new();

        readonly UserService _userService;
        readonly UnellezApiService _unellezApiService;

        string _name = string.Empty;
        string _notificationIcon = UnellezIcon.Bell;

        public string Name
        {
            get => _name;
            set => Set(ref _name, value);
        }
        public string NotificationIcon
        {
            get => _notificationIcon;
            set => Set(ref _notificationIcon, value);
        }

        public DashboardViewModel(IMainThread mainThread,
                                    IAnalyticsService analytics,
                                    IMessageService messageService,
                                    AuthenticationService authenticationService,
                                    UnellezApiService unellezApiService,
                                    UserService userService) : base(mainThread, messageService, analytics, authenticationService)
        {
            _userService = userService;
            _unellezApiService = unellezApiService;
            UserService.NameChanged += UserService_NameChanged;
            Name = _userService.Name;
        }

        void UserService_NameChanged(object sender, string e)
        {
            Name = e;
        }

        public static event EventHandler NotificationChanged
        {
            add => _notificationChangedEventManager.AddEventHandler(value);
            remove => _notificationChangedEventManager.RemoveEventHandler(value);
        }

        public IAsyncCommand LogoutCommand
        {
            get => new AsyncCommand(async () => await RunSafe(Logout(), true, BasePageConstants.Loading));
        }

        public async override Task Refresh()
        {
            var token = await _userService.GetAuthToken().ConfigureAwait(false);
            var userInfo = await _unellezApiService.GetUserInfo(token, cancellationToken: CancellationToken.None).ConfigureAwait(false);
            _userService.User = userInfo;
        }

        async Task Logout()
        {
            await _authenticationService.Logout(CancellationToken.None);
        }

        public override async Task OnAppearing()
        {
            await base.OnAppearing().ConfigureAwait(false);
            if (string.IsNullOrEmpty(Name) || _userService.User is null)
            {
                await RefreshCommand.ExecuteAsync().ConfigureAwait(false);
            }
            ComproveNotifications();
        }

        void ComproveNotifications()
        {
            NotificationIcon = UnellezIcon.Notification;
            OnNotificationChanged(new EventArgs());
        }

        void OnNotificationChanged(EventArgs e) =>
            _notificationChangedEventManager.RaiseEvent(this, e, nameof(NotificationChanged));
    }
}
