﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Refit;
using TinyMvvm;
using UnellezApp.Mobile.Common;
using UnellezApp.Mobile.Common.Constants;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.CommunityToolkit.ObjectModel;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.ViewModels
{
    public abstract class MainViewModel : ViewModelBase
    {
        readonly static WeakEventManager<PullToRefreshFailedEventArgs> _pullToRefreshFailedEventManager = new();

        protected IMessageService _messageService;
        protected AuthenticationService _authenticationService;

        public MainViewModel(IMainThread mainThread,
                            IMessageService messageService,
                            IAnalyticsService analyticsService,
                            AuthenticationService authenticationService)
        {
            MainThread = mainThread;
            _authenticationService = authenticationService;
            _messageService = messageService;
            AnalyticsService = analyticsService;
        }

        protected IAnalyticsService AnalyticsService { get; }
        protected IMainThread MainThread { get; }

        protected async Task RunSafe(Task task, bool ShowLoading = true, string? loadingMessage = null, Action<ErrorResponse>? handleError = null)
        {
            try
            {
                if (ShowLoading) {
                    MainThread.BeginInvokeOnMainThread(() => IsBusy = true);
                    _messageService.ShowLoading(loadingMessage ?? BasePageConstants.Loading);
                }

                await task;
            }
            catch(Exception ex) when (ex is ApiException apiException && (int)apiException.StatusCode == 422)
            {
                var errorResponse = await apiException.GetContentAsAsync<ErrorResponse>()!;
                if (errorResponse != null)
                    foreach (var error in errorResponse.Errors)
                        handleError?.Invoke(errorResponse);
            }
            catch (Exception ex) when (ex is ApiException apiException && apiException.StatusCode == HttpStatusCode.Unauthorized)
            {
                await _authenticationService.ForceLogout(CancellationToken.None).ConfigureAwait(false);

                handleError?.Invoke(new ErrorResponse(BasePageConstants.LoginExpired, new System.Collections.Generic.Dictionary<string, string[]>() {
                        { "internet", new[]{ BasePageConstants.PleaseLoginAgain } }
                    })
                { EventArgs = new LoginExpiredPullToRefreshEventArgs() });
            }
            catch (NotInternetException ex)
            {
                AnalyticsService.Report(ex);
                handleError?.Invoke(new ErrorResponse(BasePageConstants.UnableToConnectToUnellez, new System.Collections.Generic.Dictionary<string, string[]>() {
                        { "internet", new[]{ BasePageConstants.ReviewYourInternetConnection } }
                    }));
            }
            catch (NullReferenceException ex)
            {
                AnalyticsService.Report(ex);
                handleError?.Invoke(new ErrorResponse(BasePageConstants.ErrorGeneral, new System.Collections.Generic.Dictionary<string, string[]>() {
                        { "internet", new[]{ BasePageConstants.ErrorGeneral } }
                    }));
            }
            catch (Exception ex)
            {
                AnalyticsService.Report(ex);
                handleError?.Invoke(new ErrorResponse(BasePageConstants.ReviewYourInternetConnection, new System.Collections.Generic.Dictionary<string, string[]>() {
                        { "internet", new[]{ BasePageConstants.ErrorGeneral } }
                    }));
            }
            finally
            {
                MainThread.BeginInvokeOnMainThread(() => IsBusy = false);
                if (ShowLoading) _messageService.HideLoading();
            }
        }

        public override async Task Initialize()
        {
            if (!IsInitialized && !IsBusy)
                await RunSafe(Refresh(), false).ConfigureAwait(false);
        }

        public static event EventHandler<PullToRefreshFailedEventArgs> PullToRefreshFailed
        {
            add => _pullToRefreshFailedEventManager.AddEventHandler(value);
            remove => _pullToRefreshFailedEventManager.RemoveEventHandler(value);
        }

        public IAsyncCommand RefreshCommand
        {
            get => new AsyncCommand(async () => await RunSafe(Refresh(), true, BasePageConstants.Loading, HandleRefreshFailed));
        }

        public virtual Task Refresh() => Task.CompletedTask;

        public virtual void HandleRefreshFailed(ErrorResponse error)
        {
            OnPullToRefreshFailed(error.EventArgs as PullToRefreshFailedEventArgs ?? new ErrorPullToRefreshEventArgs(error.Message));
        }

        void OnPullToRefreshFailed(PullToRefreshFailedEventArgs pullToRefreshFailedEventArgs) =>
            _pullToRefreshFailedEventManager.RaiseEvent(this, pullToRefreshFailedEventArgs, nameof(PullToRefreshFailed));
    }
}
