﻿using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnellezApp.Mobile.Common.Constants;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.ObjectModel;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.ViewModels
{
    public class LoginViewModel : MainViewModel
    {
        bool _emailValid;
        public bool EmailValid
        {
            get => _emailValid;
            set => Set(ref _emailValid, value);
        }

        string email = string.Empty;
        public string Email
        {
            get => email;
            set => Set(ref email, value);
        }

        string password = string.Empty;
        public string Password
        {
            get => password;
            set => Set(ref password, value);
        }

        string errorText = string.Empty;
        public string ErrorText
        {
            get => errorText;
            set => Set(ref errorText, value);
        }

        public LoginViewModel(IMainThread mainThread,
                            IAnalyticsService analytics,
                            IMessageService messageService,
                            AuthenticationService authenticationService) : base(mainThread, messageService, analytics, authenticationService)
        {
#if !AppStore
            Email = "newton@ldap.forumsys.com";
            Password = "password";
#endif
        }


        public IAsyncCommand LoginCommand
        {
            get => new AsyncCommand(async () => await RunSafe(Login(), true, LoginPageConstants.Loading, LoginCommandException), (x) => !IsBusy, allowsMultipleExecutions: false);
        }

        void LoginCommandException(ErrorResponse errorResponse)
        {
            var sb = new StringBuilder();

            foreach (var error in errorResponse.Errors)
            {
                var err = error.Value.Join(" ");
                sb.Append(err);
            }

            ErrorText = sb.ToString();
        }

        public async Task Login()
        {
            if (string.IsNullOrWhiteSpace(Email))
            {
                ErrorText = LoginPageConstants.MustEnterEmail;
                _messageService.Error(ErrorText);
                return;
            }
            else if (string.IsNullOrWhiteSpace(Password))
            {
                ErrorText = LoginPageConstants.MustEnterPassword;
                _messageService.Error(ErrorText);
                return;
            }else if (!EmailValid)
            {
                ErrorText = LoginPageConstants.EmailMustBeValid;
                _messageService.Error(ErrorText);
                return;
            }

            var loginResult = await _authenticationService.Login(Email, Password, CancellationToken.None).ConfigureAwait(false);
            if (loginResult != UnellezToken.Empty)
            {
                Email = string.Empty;
                Password = string.Empty;
                _messageService.Toast(LoginPageConstants.LoginSuccess);
            }
        }
    }
}
