﻿using System;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnellezApp.Mobile.Common.Constants;

namespace UnellezApp.ViewModels
{
    public class ProfileViewModel : MainViewModel
    {
        public ObservableCollection<PersonalInfoDTO> PersonalInfoList { get; set; }

        readonly UserService _userService;

        string _statusName = string.Empty;
        string _orgWorkStatus = string.Empty;
        string _locationWork = string.Empty;
        string _profileName = string.Empty;
        string _initials = string.Empty;

        public string StatusName
        {
            get => _statusName;
            set => Set(ref _statusName, value);
        }

        public string OrganizationWorkStatus
        {
            get => _orgWorkStatus;
            set => Set(ref _orgWorkStatus, value);
        }

        public string LocationWork
        {
            get => _locationWork;
            set => Set(ref _locationWork, value);
        }

        public string ProfileName
        {
            get => _profileName;
            set => Set(ref _profileName, value);
        }

        public string? AvatarImage { get; set; }

        public string Initials
        {
            get => _initials;
            set => Set(ref _initials, value);
        }


        public ProfileViewModel(IMainThread mainThread,
                                IMessageService messageService,
                                IAnalyticsService analyticsService,
                                AuthenticationService authenticationService,
                                UserService userService) : base(mainThread, messageService, analyticsService, authenticationService)
        {
            _userService = userService;
            PersonalInfoList = new ObservableCollection<PersonalInfoDTO>();
            AssignValues();
            UserService.NameChanged += UserService_NameChanged;
        }

        void UserService_NameChanged(object sender, string e)
        {
            AssignValues();
        }

        void AssignValues()
        {
            var u = _userService.User;
            if (u is not null)
            {
                AvatarImage = "https://res.cloudinary.com/yyelda/image/upload/v1620183402/unellez/bald-eagle-550804_640.jpg";
                Initials = GetProfileNameInitials();
                ProfileName = GetProfileName();
                LocationWork = u.Vicerectorado;
                OrganizationWorkStatus = u.Nomina;
                StatusName = u.Estatus;
                PersonalInfoList = new ObservableCollection<PersonalInfoDTO>() {
                new PersonalInfoDTO(ProfilePageConstants.TitleInfoDepartment, u.Departamento),
                new PersonalInfoDTO(ProfilePageConstants.TitleCurrentJob, u.Cargo),
                };
            }
        }

        string GetProfileName()
        {
            var name = _userService.User?.Nombres.Split(" ").FirstOrDefault() ?? string.Empty;
            var lastname = _userService.User?.Apellidos.Split(" ").FirstOrDefault() ?? string.Empty;

            return $"{name} {lastname}".ToPascalCase();
        }

        string GetProfileNameInitials()
        {
            var name = GetProfileName().Split(" ").Select(x => x.FirstOrDefault());

            return $"{name.Join(string.Empty)}";
        }
    }
}
