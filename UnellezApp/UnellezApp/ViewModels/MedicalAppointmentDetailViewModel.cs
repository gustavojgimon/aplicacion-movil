﻿using System.Threading;
using System.Threading.Tasks;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.ViewModels
{
    public class MedicalAppointmentDetailViewModel : MainViewModel
    {
        readonly UnellezApiService _unellezApiService;
        readonly UserService _userService;

        string _appointmentId = string.Empty;

        MedicalAppointment _appointment = MedicalAppointment.Empty;
        public MedicalAppointment Appointment
        {
            get => _appointment;
            set => Set(ref _appointment, value);
        }

        public MedicalAppointmentDetailViewModel(IMainThread mainThread,
                                IMessageService messageService,
                                IAnalyticsService analyticsService,
                                AuthenticationService authenticationService,
                                UserService userService,
                                UnellezApiService unellezApiService) : base(mainThread, messageService, analyticsService, authenticationService)
        {
            _unellezApiService = unellezApiService;
            _userService = userService;
        }

        public async override Task Initialize()
        {
            if (NavigationParameter is MedicalAppointmentDTO appointmentDTO)
                Appointment = appointmentDTO;
            else if (NavigationParameter is string appointmentId)
            {
                _appointmentId = appointmentId;
                await RunSafe(Refresh()).ConfigureAwait(false);
            }
        }

        public async override Task Refresh()
        {
            var token = await _userService.GetAuthToken().ConfigureAwait(false);
            var appointment = await _unellezApiService.GetMedicalAppointment(token, _appointmentId, CancellationToken.None).ConfigureAwait(false);
            Appointment = appointment;
        }
    }
}
