﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;
using System.Threading.Tasks;
using System.Threading;

namespace UnellezApp.ViewModels
{
    public class NotificationViewModel : MainViewModel
    {

        readonly UnellezApiService _unellezApiService;
        readonly UserService _userService;

        IReadOnlyList<NotificationGroupedDTO> _notifications = Array.Empty<NotificationGroupedDTO>();

        public NotificationViewModel(IMainThread mainThread,
                                IMessageService messageService,
                                IAnalyticsService analyticsService,
                                AuthenticationService authenticationService,
                                UserService userService,
                                UnellezApiService unellezApiService) : base(mainThread, messageService, analyticsService, authenticationService)
        {
            _userService = userService;
            _unellezApiService = unellezApiService;
        }

        public IReadOnlyList<NotificationGroupedDTO> Notifications
        {
            get => _notifications;
            set => Set(ref _notifications, value);
        }

        public override async Task Initialize()
        {
            if (!IsInitialized && !IsBusy)
                await RunSafe(Refresh(), true).ConfigureAwait(false);
        }

        public async override Task Refresh()
        {
            var token = await _userService.GetAuthToken().ConfigureAwait(false);
            var notificationsList = await _unellezApiService.GetNotifications(token, CancellationToken.None).ConfigureAwait(false);

            Notifications = GroupByDate(notificationsList);
        }

        List<NotificationGroupedDTO> GroupByDate(IEnumerable<Notification> notifications)
        {
            var _notifications = new List<NotificationGroupedDTO>();
            foreach (var item in notifications.Where(x=>x.Priority == 0).GroupBy(x => x.Date))
            {
                if (_notifications.Any(x => x.Date.Day == item.Key.Day && x.Date.Month == item.Key.Month && x.Date.Year == item.Key.Year))
                {
                    _notifications.SingleOrDefault(x => x.Date.Day == item.Key.Day && x.Date.Month == item.Key.Month && x.Date.Year == item.Key.Year)?.AddRange(item.ToList());
                }
                else
                {
                    _notifications.Add(new NotificationGroupedDTO(new DateTime(item.Key.Year, item.Key.Month, item.Key.Day), item.ToList()));
                }
            }

            return _notifications;
        }

    }
}
