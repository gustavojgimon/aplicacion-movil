﻿using System;
namespace UnellezApp
{
    public static class UnellezIcon
    {
        public const string Logout = "\uf100";
        public const string Ecology = "\uf101";
        public const string Bell = "\uf102";
        public const string Notification = "\uf103";
        public const string Person = "\uf104";
        public const string MedicalRecord = "\uf105";
        public const string MedicalCalendar = "\uf106";
    }
}
