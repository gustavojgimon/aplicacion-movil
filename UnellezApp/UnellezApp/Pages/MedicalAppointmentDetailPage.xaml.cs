﻿using System;
using System.Collections.Generic;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace UnellezApp.Pages
{
    public partial class MedicalAppointmentDetailPage
    {
        public MedicalAppointmentDetailPage(MedicalAppointmentDetailViewModel vm, IMainThread mainThread, IAnalyticsService analyticsService) : base(vm, analyticsService, mainThread)
        {
            InitializeComponent();
        }
    }
}
