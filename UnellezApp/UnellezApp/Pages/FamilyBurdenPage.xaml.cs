﻿using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.Pages
{
    public partial class FamilyBurdenPage
    {
        public FamilyBurdenPage(FamilyBurdenViewModel vm, IMainThread mainThread, IAnalyticsService analyticsService) : base(vm, analyticsService, mainThread)
        {
            InitializeComponent();
        }
    }
}
