﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using TinyMvvm.Forms;
using TinyMvvm.IoC;
using TinyNavigationHelper;
using TinyNavigationHelper.Abstraction;
using UnellezApp.Mobile.Common;
using UnellezApp.Mobile.Common.Constants;
using UnellezApp.Pages;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Markup;
using Xamarin.Essentials;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;
using static Xamarin.CommunityToolkit.Markup.GridRowsColumns;

namespace UnellezApp
{
    public class SplashScreenPage : BaseContentPage
    {
        readonly IEnumerator<string> _statusMessageEnumerator = new List<string>
        {
            SplashScreenPageConstants.Initializing,
            SplashScreenPageConstants.ConnectingToServers,
            SplashScreenPageConstants.Initializing,
            SplashScreenPageConstants.ConnectingToServers,
            SplashScreenPageConstants.Initializing,
            SplashScreenPageConstants.ConnectingToServers,
            SplashScreenPageConstants.StillWorkingOnIt,
            SplashScreenPageConstants.LetsTryItLikeThis,
            SplashScreenPageConstants.MaybeThis,
            SplashScreenPageConstants.AnotherTry,
            SplashScreenPageConstants.ItShouldntTakeThisLong,
            SplashScreenPageConstants.AreYouSureInternetConnectionIsGood
        }.GetEnumerator();

        readonly Label _loadingLabel;
        readonly Image _unellezImage;
        readonly UserService _userService;
        readonly AppInitializationService _appInitializationService;
        readonly NotificationService _notificationService;

        CancellationTokenSource _animationCancellationToken = new();

        public SplashScreenPage(IMainThread mainThread,
                                    UserService userService,
                                    IAnalyticsService analyticsService,
                                    AppInitializationService appInitializationService,
                                    NotificationService notificationService)
            : base(analyticsService, mainThread)
        {
            this.DynamicResource(BackgroundColorProperty, "UnellezImageBackgroundColor");
            _notificationService = notificationService;
            _appInitializationService = appInitializationService;
            _userService = userService;
            _statusMessageEnumerator.MoveNext();

            Content = new Grid
            {
                RowDefinitions = Rows.Define(
                    (Row.Image, Star),
                    (Row.Text, Auto),
                    (Row.BottomPadding, 50)),

                Children =
                {
                    new LoadingLabel().Center().Assign(out _loadingLabel)
                        .Row(Row.Text),

                    new UnellezImage().Center().Assign(out _unellezImage)
                        .RowSpan(All<Row>()),
                }
            };
        }

        enum Row { Image, Text, BottomPadding }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await ChangeLabelText(_statusMessageEnumerator.Current);

            _animationCancellationToken = new CancellationTokenSource();

            //Fade the Image Opacity to 1. Work around for https://github.com/xamarin/Xamarin.Forms/issues/8073
            var fadeImageTask = _unellezImage.FadeTo(1, 1000, Easing.CubicIn);
            var pulseImageTask = PulseImage();

            //Slide status label into screen
            await _loadingLabel.TranslateTo(-10, 0, 250, Easing.CubicOut);
            await _loadingLabel.TranslateTo(0, 0, 250, Easing.CubicOut);

            //Wait for Image to reach an opacity of 1
            await Task.WhenAll(fadeImageTask, pulseImageTask);
            await Task.Delay(100);

            AppInitializationService.InitializationCompleted += HandleInitializationCompleted;

            if (_appInitializationService.IsInitializationComplete)
            {
                AppInitializationService.InitializationCompleted -= HandleInitializationCompleted;

                await HandleInitializationCompleted(true);
            }
            else
            {
                Animate(_animationCancellationToken.Token);
            }
        }

        async void Animate(CancellationToken pulseCancellationToken) => await MainThread.InvokeOnMainThreadAsync(async () =>
        {
            while (!pulseCancellationToken.IsCancellationRequested)
            {
                var pulseImageTask = PulseImage();
                await Task.Delay(TimeSpan.FromMilliseconds(400));

                //Label leaves the screen
                await _loadingLabel.TranslateTo(10, 0, 100, Easing.CubicInOut);
                await _loadingLabel.TranslateTo(-DeviceDisplay.MainDisplayInfo.Width / 2, 0, 250, Easing.CubicIn);

                //Move the label to the other side of the screen
                _loadingLabel.TranslationX = DeviceDisplay.MainDisplayInfo.Width / 2;

                //Update Status Label Text
                if (!_statusMessageEnumerator.MoveNext())
                {
                    _statusMessageEnumerator.Reset();
                    _statusMessageEnumerator.MoveNext();
                }
                await ChangeLabelText(_statusMessageEnumerator.Current);

                //Label reappears on the screen
                await _loadingLabel.TranslateTo(-10, 0, 250, Easing.CubicOut);
                await _loadingLabel.TranslateTo(0, 0, 250, Easing.CubicOut);

                await pulseImageTask;
                await Task.Delay(TimeSpan.FromMilliseconds(250));
            }
        });

        Task PulseImage() => MainThread.InvokeOnMainThreadAsync(async () =>
        {
            //Image crouches down
            await _unellezImage.ScaleTo(0.95, 100, Easing.CubicInOut);
            await Task.Delay(TimeSpan.FromMilliseconds(50));

            //Image jumps
            await _unellezImage.ScaleTo(1.25, 250, Easing.CubicOut);

            //Image crashes back to the screen
            await _unellezImage.ScaleTo(1, 500, Easing.BounceOut);
        });

        Task ChangeLabelText(string text) => ChangeLabelText(new FormattedString
        {
            Spans =
            {
                new Span
                {
                    Text = text,
                    //FontFamily = FontFamilyConstants.RobotoRegular
                }
            }
        });

        Task ChangeLabelText(string title, string body) => ChangeLabelText(new FormattedString
        {
            Spans =
            {
                new Span
                {
                    Text = title,
                    FontSize = 16,
                    //FontFamily = FontFamilyConstants.RobotoBold
                },
                new Span
                {
                    Text = "\n" + body,
                    //FontFamily = FontFamilyConstants.RobotoRegular
                }
            }
        });

        Task ChangeLabelText(FormattedString formattedString) => MainThread.InvokeOnMainThreadAsync(async () =>
        {
            await _loadingLabel.FadeTo(0, 250, Easing.CubicOut);

            _loadingLabel.Text = null;
            _loadingLabel.FormattedText = formattedString;

            await _loadingLabel.FadeTo(1, 250, Easing.CubicIn);
        });

        async void HandleInitializationCompleted(object sender, InitializationCompleteEventArgs e) =>
            await HandleInitializationCompleted(e.IsInitializationSuccessful).ConfigureAwait(false);

        async Task HandleInitializationCompleted(bool isInitializationSuccessful)
        {
            _animationCancellationToken.Cancel();

            if (isInitializationSuccessful)
            {
#if !AppStore 
                await ChangeLabelText(SplashScreenPageConstants.PreviewMode, SplashScreenPageConstants.WarningsMayAppear);
                //Display Text
                await Task.Delay(TimeSpan.FromMilliseconds(1000));
#else
                await ChangeLabelText(SplashScreenPageConstants.LetsGo);
#endif
                await NavigateToNextPage();
            }
            else
            {
                await ChangeLabelText(SplashScreenPageConstants.InitializationFailed, $"\n{SplashScreenPageConstants.EnsureInternetConnectionAndLatestVersion}");

                AnalyticsService.Track("Initialization Failed");
            }

            Task NavigateToNextPage()
            {
                return MainThread.InvokeOnMainThreadAsync(async () =>
                {
                    //Explode & Fade Everything
                    var explodeImageTask = Task.WhenAll(_unellezImage.RotateTo(360, 1000, Easing.BounceOut), Content.FadeTo(0, 500, Easing.CubicIn));
                    BackgroundColor = (Color)Application.Current.Resources["PageBackgroundColor"];

                    await explodeImageTask;
                    if (_userService.IsAuthenticated)
                    {
                        Application.Current.MainPage = new BaseNavigationPage(Resolver.Resolve<DashboardPage>());
                    }
                    else
                    {
                        await _notificationService.Initialize(CancellationToken.None).ConfigureAwait(false);

                        NavigationHelper.Current.SetRootView(nameof(LoginPage), false);
                    }
                });
            }
        }

        class UnellezImage : Image
        {
            public UnellezImage()
            {
                Opacity = 0;
                Aspect = Aspect.AspectFit;
                AutomationId = SplashScreenPageAutomationIds.UnellezImage;
                Margin = new Thickness(40);
                this.CenterExpand().DynamicResource(SourceProperty, "UnellezImageSource");
            }
        }

        class LoadingLabel : Label
        {
            public LoadingLabel()
            {
                //Begin with Label off of the screen
                TranslationX = DeviceDisplay.MainDisplayInfo.Width / 2;

                Margin = new Thickness(10, 0);
                HorizontalTextAlignment = TextAlignment.Center;
                AutomationId = SplashScreenPageAutomationIds.StatusLabel;
                this.DynamicResource(TextColorProperty, "SplashScreenStatusColor");
            }
        }
    }
}
