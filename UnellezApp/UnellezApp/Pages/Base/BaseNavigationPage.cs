﻿using Xamarin.CommunityToolkit.Markup;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace UnellezApp
{
    public class BaseNavigationPage : Xamarin.Forms.NavigationPage
    {
        public BaseNavigationPage(Xamarin.Forms.Page root) : base(root)
        {
            this.DynamicResources((BarTextColorProperty, "NavigationBarTextColor"),
                                    (BackgroundColorProperty, "PageBackgroundColor"),
                                    (BarBackgroundColorProperty, "NavigationBarBackgroundColor"));

            On<iOS>().SetPrefersLargeTitles(true);
            On<iOS>().SetModalPresentationStyle(UIModalPresentationStyle.FormSheet);
            On<iOS>().SetPrefersHomeIndicatorAutoHidden(true);
            On<iOS>().SetModalPresentationStyle(UIModalPresentationStyle.FormSheet);
            On<iOS>().DisableTranslucentNavigationBar();
            On<iOS>().SetHideNavigationBarSeparator(true);
        }
    }
}
