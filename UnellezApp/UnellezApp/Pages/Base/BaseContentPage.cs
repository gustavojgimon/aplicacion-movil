﻿using TinyMvvm;
using TinyMvvm.Forms;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Markup;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace UnellezApp
{
    public abstract class BaseContentPage : ViewBase
    {
        protected BaseContentPage(in IAnalyticsService analyticsService,
                                    in IMainThread mainThread,
                                    in bool shouldUseSafeArea = false)
        {
            MainThread = mainThread;
            AnalyticsService = analyticsService;

            this.DynamicResource(BackgroundColorProperty, "PageBackgroundColor");

            On<iOS>().SetModalPresentationStyle(UIModalPresentationStyle.FormSheet);
            On<iOS>().SetPrefersHomeIndicatorAutoHidden(true);
            On<iOS>().SetUseSafeArea(shouldUseSafeArea);
        }

        protected IAnalyticsService AnalyticsService { get; }
        protected IMainThread MainThread { get; }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            AnalyticsService.Track($"{GetType().Name} Appeared");
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            AnalyticsService.Track($"{GetType().Name} Disappeared");
        }
    }

    public abstract class BaseMvvmContentPage<T> : BaseContentPage where T : ViewModelBase
    {
        protected BaseMvvmContentPage(in T viewModel, in IAnalyticsService analyticsService, in IMainThread mainThread, in bool shouldUseSafeArea = false)
            : base(analyticsService, mainThread, shouldUseSafeArea)
        {
            BindingContext = ViewModel = viewModel;
        }

        protected T ViewModel { get; }
    }
}
