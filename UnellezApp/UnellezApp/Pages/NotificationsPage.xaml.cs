﻿using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.Pages
{
    public partial class NotificationsPage
    {
        public NotificationsPage(NotificationViewModel vm, IMainThread mainThread, IAnalyticsService analyticsService) : base(vm, analyticsService, mainThread)
        {
            InitializeComponent();
        }
    }
}
