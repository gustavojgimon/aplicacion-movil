﻿using System;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.Pages
{
    public partial class MedicalAppointmentsPage
    {
        public MedicalAppointmentsPage(MedicalAppointmentsViewModel vm, IMainThread mainThread, IAnalyticsService analyticsService) : base(vm, analyticsService, mainThread)
        {
            InitializeComponent();
        }
    }
}
