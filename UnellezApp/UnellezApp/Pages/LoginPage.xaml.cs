﻿using System;
using System.Threading.Tasks;
using TinyMvvm.IoC;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace UnellezApp.Pages
{
    public partial class LoginPage
    {
        public LoginPage(LoginViewModel loginViewModel, IMainThread mainThread, IAnalyticsService analyticsService) : base(loginViewModel, analyticsService, mainThread)
        {
            InitializeComponent();

            AuthenticationService.AuthorizeSessionCompleted += HandleAuthorizeSessionCompleted;
        }

        async void HandleAuthorizeSessionCompleted(object sender, AuthorizeSessionCompletedEventArgs e)
        {
            if (e.IsSessionAuthorized)
                await DashboardPage();
        }

        Task DashboardPage() =>
            MainThread.InvokeOnMainThreadAsync(() => Application.Current.MainPage = new BaseNavigationPage(Resolver.Resolve<DashboardPage>()));


        void Label_PropertyChanged(System.Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Label? label = sender as Label;
            if (label != null && e.PropertyName == Label.TextProperty.PropertyName)
            {
                if (!string.IsNullOrEmpty(label.Text))
                {
                    Animate(label);
                }
            }
        }

        async void Animate(Label _label) => await MainThread.InvokeOnMainThreadAsync(async () =>
        {
            //Label reappears on the screen
            await _label.TranslateTo(-10, 0, 250, Easing.CubicOut);
            await _label.TranslateTo(0, 0, 250, Easing.CubicOut);

            await Task.Delay(TimeSpan.FromMilliseconds(2000));

            //Label leaves the screen
            await _label.TranslateTo(10, 0, 100, Easing.CubicInOut);
            await _label.TranslateTo(-DeviceDisplay.MainDisplayInfo.Width / 2, 0, 250, Easing.CubicIn);
            _label.Text = string.Empty;
            //Move the label to the other side of the screen
            _label.TranslationX = DeviceDisplay.MainDisplayInfo.Width / 2;
        });
    }
}
