﻿using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.Pages
{
    public partial class ProfilePage
    {
        public ProfilePage(ProfileViewModel vm, IMainThread mainThread, IAnalyticsService analyticsService) : base(vm, analyticsService, mainThread)
        {
            InitializeComponent();
        }
    }
}
