﻿using System;
using System.Globalization;
using UnellezApp.Shared;
using Xamarin.Forms;

namespace UnellezApp
{
    public class NotificationIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string type)
            {
                return type switch
                {
                    NotificationConstants.APPOINTMENT_CREATED => UnellezIcon.MedicalRecord,
                    NotificationConstants.APPOINTMENT_REMINDER => UnellezIcon.MedicalCalendar,
                    NotificationConstants.INFORMATION => UnellezIcon.Notification,
                    _ => UnellezIcon.Bell
                };
            }
            return UnellezIcon.Bell;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }

    public class NotificationColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string type)
            {
                return type switch
                {
                    NotificationConstants.APPOINTMENT_CREATED => (Color)Application.Current.Resources["SecondaryHexColor"],
                    NotificationConstants.APPOINTMENT_REMINDER => (Color)Application.Current.Resources["PrimaryHexColor"],
                    NotificationConstants.INFORMATION => (Color)Application.Current.Resources["InfoHexColor"],
                    _ => (Color)Application.Current.Resources["SecondaryHexColor"]
                };
            }
            return (Color)Application.Current.Resources["SecondaryHexColor"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }

    public class NotificationBackgroundColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string type)
            {
                return type switch
                {
                    NotificationConstants.APPOINTMENT_CREATED => (Color)Application.Current.Resources["SecondaryHexColor_3OOpacity"],
                    NotificationConstants.APPOINTMENT_REMINDER => (Color)Application.Current.Resources["PrimaryHexColor_3OOpacity"],
                    NotificationConstants.INFORMATION => (Color)Application.Current.Resources["InfoHexColor_3OOpacity"],
                    _ => (Color)Application.Current.Resources["SecondaryHexColor_3OOpacity"]
                };
            }
            return (Color)Application.Current.Resources["SecondaryHexColor_3OOpacity"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
