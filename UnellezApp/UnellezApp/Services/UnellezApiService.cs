﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp
{
    public class UnellezApiService : MobileBaseApiService
    {
        readonly IUserApi _userApi;
        public UnellezApiService(IAnalyticsService analyticsService, IMainThread mainThread, IUserApi userApi, IConnectivity connectivity) : base(analyticsService, mainThread, connectivity)
        {
            _userApi = userApi;
        }

        public Task<UnellezToken> Login(User user, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(async() => {
            await _userApi.CsrfCookie().ConfigureAwait(false);
            return await _userApi.Login(user).ConfigureAwait(false);

        }, cancellationToken);

        public Task<UserInfo> GetUserInfo(UnellezToken token, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(() => _userApi.GetUserInfo(GetUnellezBearerTokenHeader(token)), cancellationToken);

        public Task<List<MedicalAppointment>> GetMedicalAppointments(UnellezToken token, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(() => _userApi.GetMedicalAppointments(GetUnellezBearerTokenHeader(token)), cancellationToken);

        public Task<MedicalAppointment> GetMedicalAppointment(UnellezToken token, string appointmentId, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(() => _userApi.GetMedicalAppointment(GetUnellezBearerTokenHeader(token), appointmentId), cancellationToken);

        public Task<List<Notification>> GetNotifications(UnellezToken token, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(() => _userApi.GetNotifications(GetUnellezBearerTokenHeader(token)), cancellationToken);

        public Task<List<FamilyBurden>> GetFamilyBurder(UnellezToken token, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(() => _userApi.GetFamilyBurder(GetUnellezBearerTokenHeader(token)), cancellationToken);

        public Task<int> Logout(UnellezToken token, CancellationToken cancellationToken) => AttemptAndRetry_Mobile(() => _userApi.Logout(GetUnellezBearerTokenHeader(token)), cancellationToken);

    }
}
