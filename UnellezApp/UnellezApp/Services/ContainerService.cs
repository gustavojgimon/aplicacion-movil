﻿using System;
using System.Reflection;
using Autofac;
using TinyMvvm;
using TinyMvvm.Autofac;
using TinyMvvm.IoC;
using TinyNavigationHelper;
using TinyNavigationHelper.Forms;
using UnellezApp.Pages;
using UnellezApp.Shared;
using UnellezApp.ViewModels;
using Xamarin.Essentials.Implementation;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace UnellezApp.Services
{
    public static class ContainerService
    {
        readonly static Lazy<IContainer> _containerHolder = new(CreateContainer);

        public static IContainer Container => _containerHolder.Value;

        static IContainer CreateContainer()
        {
            var navigationHelper = new ShellNavigationHelper();

            var currentAssembly = Assembly.GetExecutingAssembly();
            navigationHelper.RegisterViewsInAssembly(currentAssembly);
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterInstance<INavigationHelper>(navigationHelper);

            //essentials
            containerBuilder.RegisterType<ConnectivityImplementation>().As<IConnectivity>().SingleInstance();
            containerBuilder.RegisterType<DeviceInfoImplementation>().As<IDeviceInfo>().SingleInstance();
            containerBuilder.RegisterType<PreferencesImplementation>().As<IPreferences>().SingleInstance();
            containerBuilder.RegisterType<SecureStorageImplementation>().As<ISecureStorage>().SingleInstance();
            containerBuilder.RegisterType<MainThreadImplementation>().As<IMainThread>().SingleInstance();

            //services
            containerBuilder.RegisterType<App>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<AppInitializationService>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<NotificationService>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<ThemeService>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<LanguageService>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<MessageService>().As<IMessageService>().SingleInstance();
            containerBuilder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            containerBuilder.RegisterType<AnalyticsService>().As<IAnalyticsService>().SingleInstance();
            containerBuilder.RegisterType<UserService>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<UnellezApiService>().AsSelf().SingleInstance();
            containerBuilder.RegisterType<AuthenticationService>().AsSelf().SingleInstance();
#if !AppStore
            containerBuilder.RegisterType<UITestsBackdoorService>().AsSelf().SingleInstance();
#endif

            //Pages
            var appAssembly = typeof(App).GetTypeInfo().Assembly;
            containerBuilder.RegisterAssemblyTypes(appAssembly)
                   .Where(x => x.IsSubclassOf(typeof(Page))).AsSelf();
            //Viewmodels
            containerBuilder.RegisterAssemblyTypes(appAssembly)
                   .Where(x => x.IsSubclassOf(typeof(ViewModelBase))).AsSelf();

            //Register Refit Services
            IUserApi userApiClient = RefitExtensions.For<IUserApi>(BaseApiService.GetApi(Fusillade.Priority.UserInitiated));

            containerBuilder.RegisterInstance(userApiClient).SingleInstance();


            var container = containerBuilder.Build();

            Resolver.SetResolver(new AutofacResolver(container));

            return container;
        }
    }
}
