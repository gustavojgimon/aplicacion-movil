﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Shiny;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Helpers;

namespace UnellezApp
{
    public class AppInitializationService : IDisposable
    {
        readonly static WeakEventManager<InitializationCompleteEventArgs> _initializationCompletedEventManager = new();

        IDisposable notificationSubs;
        readonly ThemeService _themeService;
        readonly LanguageService _languageService;
        readonly IAnalyticsService _analyticsService;
        readonly NotificationService _notificationService;

        public AppInitializationService(LanguageService languageService,
                                        ThemeService themeService,
                                        IAnalyticsService analyticsService,
                                        //,
                                        NotificationService notificationService
                                        //IDeviceNotificationsService deviceNotificationService
                                        )
        {
            _themeService = themeService;
            _languageService = languageService;
            _analyticsService = analyticsService;
            _notificationService = notificationService;
            //_deviceNotificationsService = deviceNotificationService;
        }

        public static event EventHandler<InitializationCompleteEventArgs> InitializationCompleted
        {
            add => _initializationCompletedEventManager.AddEventHandler(value);
            remove => _initializationCompletedEventManager.RemoveEventHandler(value);
        }

        public bool IsInitializationComplete { get; private set; }

        public async Task<bool> InitializeApp(CancellationToken cancellationToken)
        {
            bool isInitializationSuccessful = false;

            try
            {
                #region First, Initialize Services That Dont Require API Response
                _languageService.Initialize();
                //_deviceNotificationsService.Initialize();
                await _themeService.Initialize().ConfigureAwait(false);
                #endregion

                #region Then, Initialize Services Requiring API Response
                //await _notificationService.Initialize(cancellationToken).ConfigureAwait(false);
                #endregion

                isInitializationSuccessful = true;
            }
            catch (Exception e)
            {
                _analyticsService.Report(e);
            }
            finally
            {
                OnInitializationCompleted(isInitializationSuccessful);
            }

            return isInitializationSuccessful;
        }

        void OnInitializationCompleted(bool isInitializationSuccessful)
        {
            IsInitializationComplete = isInitializationSuccessful;
            _initializationCompletedEventManager.RaiseEvent(this, new InitializationCompleteEventArgs(isInitializationSuccessful), nameof(InitializationCompleted));
        }

        public void Dispose()
        {
            notificationSubs.Dispose();
        }
    }
}
