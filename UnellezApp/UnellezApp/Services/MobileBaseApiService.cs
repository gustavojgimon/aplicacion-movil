﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using UnellezApp.Mobile.Common;
using UnellezApp.Shared;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp.Services
{
    public class MobileBaseApiService : BaseApiService
    {
        readonly IMainThread _mainThread;
        readonly IConnectivity _connectivity;
        static int _networkIndicatorCount;

        protected MobileBaseApiService(IAnalyticsService analyticsService, IMainThread mainThread, IConnectivity connectivity) =>
            (AnalyticsService, _mainThread, _connectivity) = (analyticsService, mainThread, connectivity);

        protected IAnalyticsService AnalyticsService { get; }

        protected string GetUnellezBearerTokenHeader(UnellezToken token) => $"{token.TokenType} {token.AccessToken}";

        protected async Task<T> AttemptAndRetry_Mobile<T>(Func<Task<T>> action, CancellationToken cancellationToken, int numRetries = 3, IDictionary<string, string>? properties = null, [CallerMemberName] string callerName = "")
        {
            await UpdateActivityIndicatorStatus(true).ConfigureAwait(false);

            try
            {
                using var timedEvent = AnalyticsService.TrackTime(callerName, properties);
                if (_connectivity.NetworkAccess == Xamarin.Essentials.NetworkAccess.Internet)
                    return await AttemptAndRetry(action, cancellationToken, numRetries).ConfigureAwait(false);
                throw new NotInternetException(_connectivity.NetworkAccess);
            }
            finally
            {
                await UpdateActivityIndicatorStatus(false).ConfigureAwait(false);
            }
        }

        async ValueTask UpdateActivityIndicatorStatus(bool isActivityIndicatorDisplayed)
        {
            if (isActivityIndicatorDisplayed)
            {
                _networkIndicatorCount++;

                await setIsBusy(true).ConfigureAwait(false);
            }
            else if (--_networkIndicatorCount <= 0)
            {
                _networkIndicatorCount = 0;

                await setIsBusy(false).ConfigureAwait(false);
            }

            async ValueTask setIsBusy(bool isBusy)
            {
                if (Xamarin.Forms.Application.Current?.MainPage is Xamarin.Forms.Page mainPage)
                {
                    if (mainPage.IsBusy != isBusy)
                        await _mainThread.InvokeOnMainThreadAsync(() => mainPage.IsBusy = isBusy).ConfigureAwait(false);
                }
                    
            }
        }
    }
}