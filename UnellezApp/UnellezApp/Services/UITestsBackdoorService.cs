﻿#if !AppStore
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TinyMvvm.IoC;
using UnellezApp.Pages;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Forms;

namespace UnellezApp.Services
{
    public class UITestsBackdoorService
    {
        readonly static DelegateWeakEventManager _popPageStartedEventManager = new();
        readonly static WeakEventManager<Page> _popPageCompletedEventManager = new();

        readonly UserService _userService;
        readonly UnellezApiService _unellezApiService;

        public UITestsBackdoorService(UserService userService,
                                        UnellezApiService unellezApiService)
        {
            _userService = userService;
            _unellezApiService = unellezApiService;
        }

        public static event EventHandler PopPageStarted
        {
            add => _popPageStartedEventManager.AddEventHandler(value);
            remove => _popPageStartedEventManager.RemoveEventHandler(value);
        }

        public static event EventHandler<Page> PopPageCompleted
        {
            add => _popPageCompletedEventManager.AddEventHandler(value);
            remove => _popPageCompletedEventManager.RemoveEventHandler(value);
        }

        public async Task SetUnellezUser(string token, CancellationToken cancellationToken)
        {
            await _userService.SaveToken(new UnellezToken(token, "Bearer")).ConfigureAwait(false);
            var authToken = await _userService.GetAuthToken();
            var userInfo = await _unellezApiService.GetUserInfo(authToken, CancellationToken.None);
            _userService.User = userInfo;
        }

        public Task<UnellezToken> GetAuthToken() => _userService.GetAuthToken();

        public async Task PopPage()
        {
            OnPopPageStarted();

            Page pagePopped;

            if (GetVisiblePageFromModalStack() is Page page)
                pagePopped = await page.Navigation.PopModalAsync();
            else
                pagePopped = await GetVisiblePageFromNavigationStack().Navigation.PopAsync();

            OnPopPageCompleted(pagePopped);
        }

        public Task CloseLoginPage()
        {
            var dashBoard = Resolver.Resolve<DashboardPage>();

            Application.Current.MainPage = new BaseNavigationPage(dashBoard);

            return Task.CompletedTask;
        }

        Page? GetVisiblePageFromModalStack() => Application.Current.MainPage.Navigation.ModalStack.LastOrDefault();
        Page GetVisiblePageFromNavigationStack() => Application.Current.MainPage.Navigation.NavigationStack.Last();

        void OnPopPageStarted() => _popPageStartedEventManager.RaiseEvent(this, EventArgs.Empty, nameof(PopPageStarted));
        void OnPopPageCompleted(Page page) => _popPageCompletedEventManager.RaiseEvent(this, page, nameof(PopPageCompleted));
    }
}
#endif