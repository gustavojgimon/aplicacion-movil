﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TinyMvvm.IoC;
using TinyNavigationHelper;
using UnellezApp.Pages;
using UnellezApp.Services;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Essentials;
using Xamarin.Essentials.Interfaces;

namespace UnellezApp
{
    public class AuthenticationService
    {
        readonly static DelegateWeakEventManager _loggedOuteventManager = new();
        readonly static DelegateWeakEventManager _authorizeSessionStartedEventManager = new();
        readonly static WeakEventManager<AuthorizeSessionCompletedEventArgs> _authorizeSessionCompletedEventManager = new();

        readonly IPreferences _preferences;
        readonly IAnalyticsService _analyticsService;
        readonly UserService _userService;
        readonly UnellezApiService _unellezApiService;
        readonly IDeviceInfo _deviceInfo;
        readonly INavigationService _navigationService;

        public AuthenticationService(IPreferences preferences,
                                    IAnalyticsService analyticsService,
                                    UserService userService,
                                    UnellezApiService unellezApiService,
                                    IDeviceInfo deviceInfo,
                                    INavigationService navigationService)
        {
            _navigationService = navigationService;
            _preferences = preferences;
            _analyticsService = analyticsService;
            _userService = userService;
            _deviceInfo = deviceInfo;
            _unellezApiService = unellezApiService;
        }

        public static event EventHandler AuthorizeSessionStarted
        {
            add => _authorizeSessionStartedEventManager.AddEventHandler(value);
            remove => _authorizeSessionStartedEventManager.RemoveEventHandler(value);
        }

        public static event EventHandler<AuthorizeSessionCompletedEventArgs> AuthorizeSessionCompleted
        {
            add => _authorizeSessionCompletedEventManager.AddEventHandler(value);
            remove => _authorizeSessionCompletedEventManager.RemoveEventHandler(value);
        }

        public static event EventHandler LoggedOut
        {
            add => _loggedOuteventManager.AddEventHandler(value);
            remove => _loggedOuteventManager.RemoveEventHandler(value);
        }

        string MostRecentSessionId
        {
            get => _preferences.Get(Helpers.Secrets.MostRecentSessionId, string.Format("{0}|{1}|{2}|{3}|{4:N}", _deviceInfo.Model, _deviceInfo.VersionString, _deviceInfo.Platform, DateTime.Now.Ticks, Guid.NewGuid()));
            set => _preferences.Set(Helpers.Secrets.MostRecentSessionId, value);
        }

        public async Task<UnellezToken?> Login(string email, string pass, CancellationToken cts)
        {
            OnAuthorizeSessionStarted();
            UnellezToken token = UnellezToken.Empty;
            
            var deviceId = MostRecentSessionId;
            var user = new User(email, pass, deviceId);
            token = await _unellezApiService.Login(user, CancellationToken.None).ConfigureAwait(false);

            if (token != null && token != UnellezToken.Empty)
            {
                MostRecentSessionId = deviceId;

                await _userService.SaveToken(token).ConfigureAwait(false);

                var userInfo = await _unellezApiService.GetUserInfo(token, CancellationToken.None);

                _userService.User = userInfo;

                OnAuthorizeSessionCompleted(true);

                return token;
            }


            OnAuthorizeSessionCompleted(false);

            return token;
        }

        public async Task Logout(CancellationToken cts)
        {
            var token = await _userService.GetAuthToken().ConfigureAwait(false);
            await _unellezApiService.Logout(token, cts).ConfigureAwait(false);
            await ForceLogout(CancellationToken.None);
        }

        public async Task ForceLogout(CancellationToken cts)
        {
            _userService.Name = string.Empty;
            _userService.User = null;
            _userService.InvalidateToken();
            await _navigationService.SetLoginPage().ConfigureAwait(false);
            //await _userDatabase.DeleteAllData().ConfigureAwait(false);
            OnLoggedOut();
        }

        void OnAuthorizeSessionCompleted(bool isSessionAuthorized) =>
           _authorizeSessionCompletedEventManager.RaiseEvent(this, new AuthorizeSessionCompletedEventArgs(isSessionAuthorized), nameof(AuthorizeSessionCompleted));

        void OnAuthorizeSessionStarted() =>
           _authorizeSessionStartedEventManager.RaiseEvent(this, EventArgs.Empty, nameof(AuthorizeSessionStarted));

        void OnLoggedOut() => _loggedOuteventManager.RaiseEvent(this, EventArgs.Empty, nameof(LoggedOut));
    }
}
