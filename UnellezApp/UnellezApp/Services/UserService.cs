﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnellezApp.Shared;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace UnellezApp.Services
{
    public class UserService
    {
        readonly static WeakEventManager<string> _nameChangedEventManager = new();

        UnellezApiService _unellezApiService;
        IPreferences _preferences;
        ISecureStorage _secureStorage;
        UserInfo? _user;

        public UserService(UnellezApiService unellezApiService,
            IPreferences preferences,
            ISecureStorage secureStorage)
        {
            _unellezApiService = unellezApiService;
            _preferences = preferences;
            _secureStorage = secureStorage;


            AuthenticationService.LoggedOut += HandleLoggedOut;
            AuthenticationService.AuthorizeSessionCompleted += HandleAuthorizeSessionCompleted;
        }

        public static event EventHandler<string> NameChanged
        {
            add => _nameChangedEventManager.AddEventHandler(value);
            remove => _nameChangedEventManager.RemoveEventHandler(value);
        }

        public bool IsAuthenticated
        {
            get => _preferences.Get(Helpers.Secrets.IsAuthenticated, false);
            private set => _preferences.Set(Helpers.Secrets.IsAuthenticated, value);
        }

        public string Name
        {
            get => _preferences.Get(nameof(Name), string.Empty);
            set
            {
                if (Name != value)
                {
                    _preferences.Set(nameof(Name), value);
                    OnNameChanged(value);
                }
            }
        }

        public UserInfo? User
        {
            get => _user;
            set
            {
                if (_user != value)
                {
                    _user = value;
                    Name = _user?.Nombres.Split(" ").FirstOrDefault().ToPascalCase() ?? string.Empty;
                }
            }
        }

        public async Task SaveToken(UnellezToken token)
        {
            IsAuthenticated = false;
            if (token is null)
                throw new ArgumentNullException(nameof(token));

            if (token.AccessToken is null)
                throw new ArgumentNullException(nameof(token.AccessToken));

            var serializedToken = JsonConvert.SerializeObject(token);

            if (Device.RuntimePlatform == Device.Android)
                await _secureStorage.SetAsync(Helpers.Secrets.OAuthToken, serializedToken).ConfigureAwait(false);
            else {
                _preferences.Set(Helpers.Secrets.OAuthToken, serializedToken);
            }
            
            IsAuthenticated = true;
        }

        public async Task<UnellezToken> GetAuthToken()
        {
            var serializedToken = Device.RuntimePlatform switch {
                Device.Android => await _secureStorage.GetAsync(Helpers.Secrets.OAuthToken).ConfigureAwait(false),
                _ => _preferences.Get(Helpers.Secrets.OAuthToken, string.Empty)
            };

            try
            {
                var token = JsonConvert.DeserializeObject<UnellezToken?>(serializedToken);

                if (token is null)
                    return UnellezToken.Empty;

                IsAuthenticated = true;

                return token;
            }
            catch (ArgumentNullException)
            {
                IsAuthenticated = false;
                return UnellezToken.Empty;
            }
            catch (JsonReaderException)
            {
                IsAuthenticated = false;
                return UnellezToken.Empty;
            }
        }

        public void InvalidateToken()
        {
            _secureStorage.Remove(Helpers.Secrets.OAuthToken);
            IsAuthenticated = false;
        }

        void HandleLoggedOut(object sender, EventArgs e)
        {
            IsAuthenticated = false;
        }

        void HandleAuthorizeSessionCompleted(object sender, AuthorizeSessionCompletedEventArgs e) => IsAuthenticated = e.IsSessionAuthorized;

        void OnNameChanged(in string name) => _nameChangedEventManager.RaiseEvent(this, name, nameof(NameChanged));

    }
}
