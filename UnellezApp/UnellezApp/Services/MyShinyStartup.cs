﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Shiny;
using Shiny.Push;
using UnellezApp.Services;

namespace UnellezApp
{
    public class MyShinyStartup : ShinyStartup
    {
        public override void ConfigureServices(IServiceCollection services, IPlatform platform)
        {
            services.UseFirebaseMessaging<PushDelegate>();
            // this is where you'll load things like BLE, GPS, etc - those are covered in other sections
            // things like the jobs, environment, power, are all installed automatically

        }

        //public override void ConfigureLogging(ILoggingBuilder builder, IPlatform platform)
        //{
        //    builder.AddFirebase();
        //    builder.AddAppCenter("your appcenter key");
        //}
    }
}
