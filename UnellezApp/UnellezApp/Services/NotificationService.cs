﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Shiny;
using Shiny.Push;

namespace UnellezApp.Services
{
    public class NotificationService
    {
        public NotificationService()
        {
        }

        public async Task Initialize(CancellationToken cancellationToken)
        {
            var push = ShinyHost.Resolve<IPushManager>();
            var result = await push.RequestAccess();
            if (result.Status == AccessState.Available)
            {
                // good to go

                // you should send this to your server with a userId attached if you want to do custom work
                var value = result.RegistrationToken;
                Console.WriteLine("TOKEN: " + value);
            }
        }
    }
}
