﻿using System;
using System.Threading.Tasks;
using Shiny.Push;

namespace UnellezApp.Services
{
    public class PushDelegate : IPushDelegate
    {
        public Task OnEntry(PushNotificationResponse response) => throw new NotImplementedException();
        public Task OnReceived(PushNotification notification) => throw new NotImplementedException();
        public Task OnTokenChanged(string token) => throw new NotImplementedException();
    }
}
