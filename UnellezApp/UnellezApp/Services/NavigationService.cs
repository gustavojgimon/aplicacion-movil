﻿using System;
using System.Threading.Tasks;
using TinyMvvm.IoC;
using TinyNavigationHelper;
using TinyNavigationHelper.Abstraction;
using UnellezApp.Pages;
using UnellezApp.Shared;
using Xamarin.Essentials;

namespace UnellezApp
{
    public class NavigationService : INavigationService
    {
        public async Task SetLoginPage() =>
            await MainThread.InvokeOnMainThreadAsync(() => NavigationHelper.Current.SetRootView(nameof(LoginPage), false));
    }
}
