﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Xamarin.Forms;
using UnellezApp.Services;
using Autofac;
using Microsoft.Extensions.DependencyInjection;
using Shiny;

[assembly: ShinyApplication(
    ShinyStartupTypeName = "UnellezApp.MyShinyStartup",
    XamarinFormsAppTypeName = "UnellezApp.App"
)]
namespace UnellezApp.Droid
{
    [Activity(Label = "Unellez", Icon = "@mipmap/ic_launcher", RoundIcon = "@mipmap/ic_launcher_round", Theme = "@style/LaunchTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize)]
    public partial class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.SetTheme(Resource.Style.MainTheme);
            base.OnCreate(savedInstanceState);

            Forms.Init(this, savedInstanceState);
            var app = ContainerService.Container.Resolve<App>();
            LoadApplication(app);
        }
    }
}