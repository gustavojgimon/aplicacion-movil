﻿#if !AppStore
using System.Threading;
using Android.Runtime;
using Java.Interop;
using TinyMvvm.IoC;
using UnellezApp.Mobile.Common;
using UnellezApp.Services;

namespace UnellezApp.Droid
{
    public partial class MainApplication
    {
        UITestsBackdoorService? _uiTestBackdoorService;
        UITestsBackdoorService UITestBackdoorService => _uiTestBackdoorService ??= Resolver.Resolve<UITestsBackdoorService>();

        [Preserve, Export(BackdoorMethodConstants.PopPage)]
        public async void PopPage() => await UITestBackdoorService.PopPage().ConfigureAwait(false);

        [Preserve, Export(BackdoorMethodConstants.CloseLoginPage)]
        public async void CloseLoginPage(string noValue) =>
            await UITestBackdoorService.CloseLoginPage().ConfigureAwait(false);

        [Preserve, Export(BackdoorMethodConstants.SetUnellezUser)]
        public async void SetUnellezUser(string accessToken) =>
            await UITestBackdoorService.SetUnellezUser(accessToken.ToString(), CancellationToken.None).ConfigureAwait(false);

        [Preserve, Export(BackdoorMethodConstants.GetAuthToken)]
        public string GetUnellezToken() => SerializeObject(UITestBackdoorService.GetAuthToken().GetAwaiter().GetResult());

        static string SerializeObject<T>(T value) => Newtonsoft.Json.JsonConvert.SerializeObject(value);
    }
}
#endif