﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UnellezApp.Shared
{
    public record ErrorResponse
    {
        public ErrorResponse(string message, Dictionary<string, string[]> errors)
        {
            Message = message;
            Errors = errors;
        }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("errors")]
        public Dictionary<string, string[]> Errors { get; set; }

        [JsonIgnore]
        public EventArgs? EventArgs;
    }
}
