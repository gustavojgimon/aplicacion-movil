﻿using System;
namespace UnellezApp.Shared
{
    public record PersonalInfoDTO
    {
        public PersonalInfoDTO(string title, string description) => (Title, Description) = (title, description);

        public string Title { get; }

        public string Description { get; }
    }
}
