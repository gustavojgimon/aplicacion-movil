﻿namespace UnellezApp.Shared
{
    public record FamilyBurdenDTO : FamilyBurden, ISeparatorItem
    {
        public bool BottomLineVisibility { get; set; }

        public FamilyBurdenDTO(string names, string dNI, string gender, string birthDate, string relationship) : base(names, dNI, gender, birthDate, relationship)
        {
            BottomLineVisibility = true;
        }
    }
}
