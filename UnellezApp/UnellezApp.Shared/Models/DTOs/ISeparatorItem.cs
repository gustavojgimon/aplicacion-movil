﻿namespace UnellezApp.Shared
{
    public interface ISeparatorItem
    {
        public bool BottomLineVisibility { get; set; }
    }
}
