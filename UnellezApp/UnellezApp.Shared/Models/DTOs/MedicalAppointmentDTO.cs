﻿namespace UnellezApp.Shared
{
    public record MedicalAppointmentDTO : MedicalAppointment, ISeparatorItem
    {
        public bool BottomLineVisibility { get; set; }
        public MedicalAppointmentDTO(string id, string specialization, string doctorName, string date, string location) : base(id, specialization, doctorName, date, location)
        {
            BottomLineVisibility = true;
        }
    }
}
