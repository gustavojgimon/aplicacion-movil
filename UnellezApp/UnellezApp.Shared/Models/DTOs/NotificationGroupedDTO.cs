﻿using System;
using System.Collections.Generic;
using UnellezApp.Mobile.Common.Constants;

namespace UnellezApp.Shared
{
    public class NotificationGroupedDTO : List<Notification>
    {
        public DateTime Date { get; private set; }

        public string DisplayDate
        {
            get
            {
                if (Date == DateTime.Today)
                {
                    return BasePageConstants.Today;
                }

                return Date.ToString("dddd dd MMMM");
            }
        }

        public NotificationGroupedDTO(DateTime date, List<Notification> notifications) : base(notifications)
        {
            Date = date;
        }
    }
}
