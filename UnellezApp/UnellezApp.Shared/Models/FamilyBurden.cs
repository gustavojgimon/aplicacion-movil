namespace UnellezApp.Shared
{
    public record FamilyBurden
    {
        public FamilyBurden(string names, string dNI, string gender, string birthDate, string relationship)
        {
            Names = names;
            DNI = dNI;
            Gender = gender;
            BirthDate = birthDate;
            Relationship = relationship;
        }

        public int Id { get; }
        public string Names { get; }
        public string DNI { get; }
        public string Gender { get; }
        public string BirthDate { get; }
        public string Relationship { get; }
    }
}
