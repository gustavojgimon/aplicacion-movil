﻿using Newtonsoft.Json;

namespace UnellezApp.Shared
{
    public record UnellezToken
    {
        public UnellezToken(string accessToken, string tokenType) => (AccessToken, TokenType) = (accessToken, tokenType);

        [JsonProperty("access_token")]
        public string AccessToken { get; }

        [JsonProperty("token_type")]
        public string TokenType { get; }

        public static UnellezToken Empty { get; } = new UnellezToken(string.Empty, string.Empty);

    }

    public static class UnellezTokenExtensions
    {
        public static bool IsEmpty(this UnellezToken unellezToken) => unellezToken.AccessToken == string.Empty
                                                                    && unellezToken.TokenType == string.Empty;
    }
}
