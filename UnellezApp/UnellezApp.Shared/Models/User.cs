﻿using System;
using Newtonsoft.Json;

namespace UnellezApp.Shared
{
    public record User
    {
        public User(string email, string password, string deviceId)
        {
            Email = email;
            Password = password;
            DeviceId = deviceId;
        }

        [JsonProperty("email")]
        public string Email { get; }

        [JsonProperty("password")]
        public string Password { get; }

        [JsonProperty("device_id")]
        public string DeviceId { get; }
    }
}
