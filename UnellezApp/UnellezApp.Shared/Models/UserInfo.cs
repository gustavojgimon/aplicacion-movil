﻿using System;
using Newtonsoft.Json;

namespace UnellezApp.Shared
{
    public record UserInfo
    {
        public UserInfo(
            string codigo,
            string cedula,
            string nombres,
            string apellidos,
            string estatus,
            string correo,
            string nomina,
            string cargo,
            string vicerectorado,
            string departamento
        )
        {
            this.Codigo = codigo;
            this.Cedula = cedula;
            this.Nombres = nombres;
            this.Apellidos = apellidos;
            this.Estatus = estatus;
            this.Correo = correo;
            this.Nomina = nomina;
            this.Cargo = cargo;
            this.Vicerectorado = vicerectorado;
            this.Departamento = departamento;
        }

        public string Codigo { get; }
        public string Cedula { get; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Estatus { get; }
        public string Correo { get; }
        public string Nomina { get; }
        public string Cargo { get; }
        public string Vicerectorado { get; }
        public string Departamento { get; }
    }
}
