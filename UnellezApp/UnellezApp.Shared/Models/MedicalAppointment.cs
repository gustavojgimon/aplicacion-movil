using System;

namespace UnellezApp.Shared
{
    public record MedicalAppointment
    {
        public MedicalAppointment(string id, string specialization, string doctorName, string date, string location)
        {
            Id = id;
            Specialization = specialization;
            DoctorName = doctorName;
            Date = date;
            Location = location;
        }

        public string Id { get; }
        public string Specialization { get; }
        public string DoctorName { get; }
        public string Date { get; }
        public string Location { get; }

        public static MedicalAppointment Empty { get; } = new MedicalAppointment(string.Empty, string.Empty, string.Empty,string.Empty, string.Empty);
    }
}
