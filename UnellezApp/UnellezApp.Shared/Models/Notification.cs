﻿using System;
using Newtonsoft.Json;

namespace UnellezApp.Shared
{
    public record Notification
    {
        public Notification(string title, string description, DateTime date, bool isRead, string type, string idEntity, int priority)
        {
            Date = date;
            Title = title;
            Description = description;
            Type = type;
            IsRead = isRead;
            IdEntity = idEntity;
            Priority = priority;
        }

        [JsonProperty("id")]
        public Guid Id { get; }

        [JsonProperty("created_at")]
        public DateTime Date { get; }

        [JsonProperty("title")]
        public string Title { get; }

        [JsonProperty("description")]
        public string Description { get; }

        [JsonProperty("id_entity")]
        public string IdEntity { get; }

        [JsonProperty("priority")]
        public int Priority { get; }

        [JsonProperty("type")]
        public string Type { get; }

        [JsonProperty("is_read")]
        public bool IsRead { get; set; }
    }
}
