﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace UnellezApp.Shared
{
    public static class StringExtensions
    {
        public static string ToPascalCase(this string input)
        {
            var resultBuilder = new System.Text.StringBuilder();
            foreach (char c in input)
            {
                if (!char.IsLetterOrDigit(c))
                    resultBuilder.Append(" ");
                else
                    resultBuilder.Append(c);
            }

            string result = resultBuilder.ToString();
            result = result.ToLower();

            var textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(result);
        }

        public static string RemoveEmoji(this string text) => Regex.Replace(text, @"\p{Cs}", "");

        public static string Join<TItem, TSep>(
    this IEnumerable<TItem> enuml,
    TSep separator)
        {
            if (null == enuml) return string.Empty;

            var sb = new StringBuilder();

            using (var enumr = enuml.GetEnumerator())
            {
                if (null != enumr && enumr.MoveNext())
                {
                    sb.Append(enumr.Current);
                    while (enumr.MoveNext())
                    {
                        sb.Append(separator).Append(enumr.Current);
                    }
                }
            }

            return sb.ToString();
        }
    }
}
