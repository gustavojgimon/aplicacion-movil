﻿using System;
namespace UnellezApp.Shared
{
    public static class NotificationConstants
    {
        public const string APPOINTMENT_CREATED = nameof(APPOINTMENT_CREATED);
        public const string APPOINTMENT_REMINDER = nameof(APPOINTMENT_REMINDER);
        public const string INFORMATION = nameof(INFORMATION);
    }
}
