﻿using System;
using System.Threading.Tasks;

namespace UnellezApp.Shared
{
    public interface INavigationService
    {
        Task SetLoginPage();
    }
}
