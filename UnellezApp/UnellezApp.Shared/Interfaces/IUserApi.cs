﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;

namespace UnellezApp.Shared
{
    [Headers("Accept: application/json", "Accept-Encoding: gzip, deflate, br", "User-Agent: " + nameof(UnellezApp), "")]
    public interface IUserApi
    {
        [Get("/sanctum/csrf-cookie")]
        Task CsrfCookie();

        [Post("/api/v1/login")]
        Task<UnellezToken> Login([Body(BodySerializationMethod.UrlEncoded)] User user);

        [Get("/api/v1/testToken")]
        Task<UnellezToken> GetTestToken([AliasAs("token")] string functionKey = Helpers.Secrets.UITestToken);

        [Get("/api/v1/user")]
        Task<UserInfo> GetUserInfo([Header("Authorization")] string authorization);

        [Get("/api/v1/medicalappointments")]
        Task<List<MedicalAppointment>> GetMedicalAppointments([Header("Authorization")] string authorization);

        [Get("/api/v1/medicalappointment/{appointment}")]
        Task<MedicalAppointment> GetMedicalAppointment([Header("Authorization")] string authorization, [AliasAs("appointment")] string appointment);

        [Get("/api/v1/notifications")]
        Task<List<Notification>> GetNotifications([Header("Authorization")] string authorization);

        [Put("/api/v1/notification/markasread/{notification}")]
        Task MarkAsReadNotification([Header("Authorization")] string authorization, [AliasAs("notification")] string notificationId);

        [Get("/api/v1/familyburden")]
        Task<List<FamilyBurden>> GetFamilyBurder([Header("Authorization")] string authorization);

        [Post("/api/v1/logout")]
        Task<int> Logout([Header("Authorization")] string authorization);
    }
}
