﻿using System;
using System.Threading.Tasks;

namespace UnellezApp.Shared
{
    public interface IMessageService
    {
        void Toast(string message);
        void Error(params string[] message);
        void ShowLoading(string loadingMessage);
        void HideLoading();
        Task DisplayAlert(string title, string message, string buttonText);
    }
}
