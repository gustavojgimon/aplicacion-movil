﻿#if !AppStore
using System;
using System.Threading;
using Autofac;
using Foundation;
using UnellezApp.Mobile.Common;
using UnellezApp.Services;

namespace UnellezApp.iOS
{
    public partial class AppDelegate
    {
        public AppDelegate() => Xamarin.Calabash.Start();

        UITestsBackdoorService? _uiTestBackdoorService;
        UITestsBackdoorService UITestBackdoorService => _uiTestBackdoorService ??= ContainerService.Container.Resolve<UITestsBackdoorService>();

        [Preserve, Export(BackdoorMethodConstants.PopPage + ":")]
        public async void PopPage(NSString noValue) =>
            await UITestBackdoorService.PopPage().ConfigureAwait(false);

        [Preserve, Export(BackdoorMethodConstants.CloseLoginPage + ":")]
        public async void CloseLoginPage(NSString noValue) =>
            await UITestBackdoorService.CloseLoginPage().ConfigureAwait(false);

        [Preserve, Export(BackdoorMethodConstants.SetUnellezUser + ":")]
        public async void SetUnellezUser(NSString accessToken) =>
            await UITestBackdoorService.SetUnellezUser(accessToken.ToString(), CancellationToken.None).ConfigureAwait(false);

        [Preserve, Export(BackdoorMethodConstants.GetAuthToken + ":")]
        public NSString GetAuthToken(NSString noValue) =>
            SerializeObject(UITestBackdoorService.GetAuthToken().GetAwaiter().GetResult());

        static NSString SerializeObject<T>(T value) => new (Newtonsoft.Json.JsonConvert.SerializeObject(value));
    }
}
#endif
