﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnellezApp.Shared;
namespace UnellezApp.Mobile.Common
{
    public abstract class UserApiService : BaseApiService
    {
        readonly static Lazy<IUserApi> _unellezApiClientHolder = new(() => RefitExtensions.For<IUserApi>(GetApi(Fusillade.Priority.UserInitiated)));

        static IUserApi UnellezApiClient => _unellezApiClientHolder.Value;

        public static Task<UnellezToken> GetTestToken() => AttemptAndRetry(() => UnellezApiClient.GetTestToken(), CancellationToken.None);
    }
}
