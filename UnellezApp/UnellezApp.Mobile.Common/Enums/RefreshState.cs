﻿using System;
namespace UnellezApp.Mobile.Common
{
    public enum RefreshState { Uninitialized, Succeeded, LoginExpired, InternetFailed, Error }
}
