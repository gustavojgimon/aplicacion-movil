﻿using System;
namespace UnellezApp.Mobile.Common
{
    public static class BackdoorMethodConstants
    {
        public const string SetUnellezUser = nameof(SetUnellezUser);
        public const string GetAuthToken = nameof(GetAuthToken);
        public const string PopPage = nameof(PopPage);
        public const string CloseLoginPage = nameof(CloseLoginPage);
    }
}
