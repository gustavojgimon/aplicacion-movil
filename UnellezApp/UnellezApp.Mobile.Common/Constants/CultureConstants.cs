﻿using System.Collections.Generic;
using System.Linq;
using UnellezApp.Shared;

namespace UnellezApp.Mobile.Common
{
    public static class CultureConstants
    {
        static readonly IReadOnlyDictionary<string, string> _cultureOptions = new Dictionary<string, string>
        {
            { "en", "🇺🇸 English" },
            { "es", "🇪🇸 Español" },
        };

        public static IReadOnlyDictionary<string, string> CulturePickerOptions { get; } = InitializeCulturePickerOptions();

        static IReadOnlyDictionary<string, string> InitializeCulturePickerOptions()
        {
            var culturePickerOptions = new Dictionary<string, string>
            {
                {"", "Default" }
            };

            foreach (var keyValuePair in _cultureOptions.OrderBy(x => x.Value.RemoveEmoji()))
                culturePickerOptions.Add(keyValuePair.Key, keyValuePair.Value);

            return culturePickerOptions;
        }
    }
}