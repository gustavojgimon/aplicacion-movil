﻿using System;
namespace UnellezApp.Mobile.Common
{
    public static class SplashScreenPageAutomationIds
    {
        public const string UnellezImage = nameof(SplashScreenPageAutomationIds) + nameof(UnellezImage);
        public const string StatusLabel = nameof(SplashScreenPageAutomationIds) + nameof(StatusLabel);
    }
}
