﻿using System;
namespace UnellezApp.Mobile.Common
{
    public static class DashboardPageAutomationIds
    {
        public const string UserCardTitle = nameof(DashboardPageAutomationIds) + nameof(UserCardTitle);
        public const string UserCardDescription = nameof(DashboardPageAutomationIds) + nameof(UserCardDescription);
        public const string UserCardIcon = nameof(DashboardPageAutomationIds) + nameof(UserCardIcon);
        public const string ProfileCardTitle = nameof(DashboardPageAutomationIds) + nameof(ProfileCardTitle);
        public const string FamilyCardTitle = nameof(DashboardPageAutomationIds) + nameof(FamilyCardTitle);
        public const string UnellezCardImage = nameof(DashboardPageAutomationIds) + nameof(UnellezCardImage);
        public const string MedicalCardTitle = nameof(DashboardPageAutomationIds) + nameof(MedicalCardTitle);
        public const string NotificationsCardTitle = nameof(DashboardPageAutomationIds) + nameof(NotificationsCardTitle);


        public const string ProfileCard = nameof(DashboardPageAutomationIds) + nameof(ProfileCard);
        public const string FamilyCard = nameof(DashboardPageAutomationIds) + nameof(FamilyCard);
        public const string UnellezCard = nameof(DashboardPageAutomationIds) + nameof(UnellezCard);
        public const string MedicalCard = nameof(DashboardPageAutomationIds) + nameof(MedicalCard);
        public const string NotificationsCard = nameof(DashboardPageAutomationIds) + nameof(NotificationsCard);

        public const string LogoutToolbar = nameof(DashboardPageAutomationIds) + nameof(LogoutToolbar);
        public const string NotificationToolbar = nameof(DashboardPageAutomationIds) + nameof(NotificationToolbar);
    }
}
