﻿using System;
namespace UnellezApp.Mobile.Common
{
    public static class LoginPageAutomationIds
    {
        public const string EmailEntry = nameof(LoginPageAutomationIds) + nameof(EmailEntry);
        public const string PasswordEntry = nameof(LoginPageAutomationIds) + nameof(PasswordEntry);
        public const string ErrorLabel = nameof(LoginPageAutomationIds) + nameof(ErrorLabel);
        public const string SubmitButton = nameof(LoginPageAutomationIds) + nameof(SubmitButton);
        public const string ActivityIndicator = nameof(LoginPageAutomationIds) + nameof(ActivityIndicator);
        public const string UnellezImage = nameof(LoginPageAutomationIds) + nameof(UnellezImage);
        public const string TitleForm = nameof(LoginPageAutomationIds) + nameof(TitleForm);
        public const string EmailLabel = nameof(LoginPageAutomationIds) + nameof(EmailLabel);
        public const string PasswordLabel = nameof(LoginPageAutomationIds) + nameof(PasswordLabel);

    }
}
