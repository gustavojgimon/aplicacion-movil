namespace UnellezApp.Helpers
{
    public static class Secrets
    {
        /// <summary>
        /// App center key
        /// </summary>
        public const string AppCenterDroid = "7c4cd197-fb64-4f2c-b719-3f799fb1bc39;";
        /// <summary>
        /// Url for the backend api
        /// </summary>
        public const string ApiBackend = "http://desarrollo.unellez.edu.ve/unellez_app_api/WebApi/public";
        //public const string ApiBackend = "http://192.168.0.104";
        /// <summary>
        /// Nombre de la clave en UUID para guardar en las preferencias del dispositivo del ultimo id de sesion registrado
        /// </summary>
        public const string MostRecentSessionId = "f02b9fcf-8305-4e53-a2b2-c9abdcd1fa56";
        /// <summary>
        /// Nombre de la clave en UUID para guardar en el secure storage del dispositivo el token de la api
        /// </summary>
        public const string OAuthToken = "c05d4839-0368-4960-b1cc-50cb7536e187";
        /// <summary>
        /// Token de pruebas de interfaz de usuario (UnellezApp.UITest)
        /// </summary>
        public const string UITestToken = "0f774c80-855d-43c4-ab78-12c9e681a400";
        /// <summary>
        /// Nombre de la clave en UUID para guardar en las preferencias del dispositivo si esta logueado o no
        /// </summary>
        public const string IsAuthenticated = "20875a6f-341f-410a-b162-1e7ef43802c1";
    }
}
