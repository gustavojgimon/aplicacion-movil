﻿using System;
using Xamarin.Essentials;

namespace UnellezApp.Mobile.Common
{
    public class NotInternetException : Exception
    {
        public NetworkAccess NetworkAccess { get; }

        public NotInternetException(NetworkAccess networkAccess) => NetworkAccess = networkAccess;
    }
}
