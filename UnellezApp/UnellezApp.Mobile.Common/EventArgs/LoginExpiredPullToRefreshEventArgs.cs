﻿using UnellezApp.Mobile.Common.Constants;

namespace UnellezApp.Mobile.Common
{
    public class LoginExpiredPullToRefreshEventArgs : PullToRefreshFailedEventArgs
    {
        public LoginExpiredPullToRefreshEventArgs() : base(BasePageConstants.LoginExpired, BasePageConstants.PleaseLoginAgain)
        {
        }
    }
}
