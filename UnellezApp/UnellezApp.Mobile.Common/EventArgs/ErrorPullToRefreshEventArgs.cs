﻿using UnellezApp.Mobile.Common.Constants;

namespace UnellezApp.Mobile.Common
{
    public class ErrorPullToRefreshEventArgs : PullToRefreshFailedEventArgs
    {
        public ErrorPullToRefreshEventArgs(string message) : base(BasePageConstants.UnableToConnectToUnellez, message)
        {
        }
    }
}
