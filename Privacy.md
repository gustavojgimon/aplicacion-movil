# Politica de privacidad de Unellez App

Esta Aplicación recoge algunos datos personales de sus Usuarios.

## Resumen de la política

### Datos Personales recogidos para las siguientes finalidades y utilizando los siguientes servicios

Analítica

- App Center

  - Datos personales: Datos de uso

Supervisión de la infraestructura

- App Center

  - Datos personales: varios tipos de datos según se especifica en la política de privacidad del servicio.

### Más información sobre los datos personales

Notificaciones push

- Esta aplicación puede enviar notificaciones push al Usuario.

### Información de contacto

Propietario de los datos

- Daniel Tovar\
Correo electrónico de contacto del propietario: danieldaniyyelda@gmail.com

## Política completa

Responsable del tratamiento y propietario de los datos

- Daniel Tovar\
Correo electrónico de contacto del propietario: danieldaniyyelda@gmail.com

### Tipos de datos recogidos

Entre los tipos de Datos Personales que esta Aplicación recoge, por sí misma o a través de terceros, se encuentran los Datos de Uso. Otros Datos Personales recogidos pueden ser descritos en otras secciones de esta política de privacidad o mediante un texto explicativo dedicado contextualmente a la recogida de Datos. Los Datos Personales pueden ser proporcionados libremente por el Usuario, o recogidos automáticamente al utilizar esta Aplicación. El uso de herramientas de seguimiento por parte de esta Aplicación o de los propietarios de los servicios de terceros utilizados por esta Aplicación, salvo que se indique lo contrario, sirve para identificar a los Usuarios y recordar sus preferencias, con el único fin de prestar el servicio requerido por el Usuario. La falta de suministro de determinados Datos Personales puede imposibilitar la prestación de los servicios de esta Aplicación. Los Usuarios son responsables de los Datos Personales de terceros obtenidos, publicados o compartidos a través de esta Aplicación y confirman que cuentan con el consentimiento del tercero para proporcionar los Datos al Propietario.

### Modo y lugar de tratamiento de los datos

**Métodos de tratamiento** El Responsable del tratamiento procesa los Datos de los Usuarios de manera adecuada y adoptará las medidas de seguridad apropiadas para evitar el acceso no autorizado, la divulgación, la modificación o la destrucción no autorizada de los Datos.
El tratamiento de los datos se lleva a cabo mediante ordenadores y/o herramientas informáticas, siguiendo procedimientos y modos de organización estrictamente relacionados con los fines indicados. Además del Controlador de Datos, en algunos casos, los Datos pueden ser accesibles a ciertos tipos de personas encargadas, involucradas con la operación del sitio (administración, ventas, marketing, legal, administración del sistema) o partes externas (como proveedores de servicios técnicos de terceros, transportistas de correo, proveedores de alojamiento, compañías de TI, agencias de comunicaciones) designadas, si es necesario, como Procesadores de Datos por el Propietario. La lista actualizada de estas partes puede solicitarse al Responsable del tratamiento los datos en cualquier momento.

**Lugar** El tratamiento de los datos se lleva a cabo en las oficinas operativas del responsable de controlar los datos y en cualquier otro lugar donde se encuentren las partes implicadas en el tratamiento. Para más información, póngase en contacto con el Responsable del tratamiento.

**Tiempo de conservación** Los Datos se conservan durante el tiempo necesario para la prestación del servicio solicitado por el Usuario, o bien según las finalidades señaladas en este documento, pudiendo el Usuario solicitar en todo momento al Responsable del Tratamiento la suspensión o eliminación de los mismos.

### El uso de los datos recogidos

Los Datos relativos al Usuario se recogen para que el Propietario pueda prestar sus servicios, así como para los siguientes fines: Monitorización de la infraestructura, Analítica y Alojamiento e infraestructura de backend.
Los Datos Personales utilizados para cada finalidad se detallan en los apartados específicos de este documento.

### Información detallada sobre el tratamiento de datos personales

Los datos personales se recogen para los siguientes fines y mediante los siguientes servicios:

**Analitica** Los servicios contenidos en esta sección permiten al Propietario supervisar y analizar el tráfico web y pueden utilizarse para hacer un seguimiento del comportamiento del Usuario.\
App Center (Microsoft Corporation)\
App Center es un servicio de análisis proporcionado por Microsoft Corporation.\
Datos personales recogidos: Datos de uso.\
Lugar de tratamiento: EE.UU. - [Politica de privacidad](https://aka.ms/appcenterprivacy)

**Supervisión de la infraestructura** Este tipo de servicio permite a esta Aplicación monitorizar el uso y el comportamiento de sus componentes para poder mejorar su rendimiento, funcionamiento, mantenimiento y resolución de problemas.\
El tratamiento de los Datos Personales depende de las características y el modo de implementación de estos servicios, cuya función es filtrar las actividades de esta Aplicación.\
App Center (Microsoft Corporation)\
App Center es un servicio de monitorización proporcionado por Microsoft Corporation.\
Datos Personales recogidos: varios tipos de Datos según se especifica en la política de privacidad del servicio.\
Lugar de tratamiento: US - [Politica de privacidad](https://aka.ms/appcenterprivacy)

### Más informacion sobre los datos personales

**Notificaciones push**
Esta Aplicación puede enviar notificaciones push al Usuario.

### Información adicional sobre la recogida y el tratamiento de datos

**Acciones legales**
Los Datos Personales del Usuario podrán ser utilizados con fines legales por el Responsable del Tratamiento, en los Tribunales o en las fases que conduzcan a posibles acciones legales derivadas del uso indebido de esta Aplicación o de los servicios relacionados.
El Usuario declara ser consciente de que el Responsable del Tratamiento puede verse obligado a revelar los datos personales a petición de las autoridades públicas.

**Información adicional sobre los Datos Personales del Usuario**
Además de la información contenida en esta política de privacidad, esta Aplicación puede proporcionar al Usuario información adicional y contextual relativa a determinados servicios o a la recogida y tratamiento de Datos Personales si así lo solicita.

**Registros y mantenimiento del sistema**
Por motivos de funcionamiento y mantenimiento, esta Aplicación y cualquier servicio de terceros pueden recopilar archivos que registren la interacción con esta Aplicación (registros del sistema) o utilizar para ello otros Datos Personales (como la dirección IP).

**Información no contenida en esta política**
Se pueden solicitar más detalles sobre la recogida o el tratamiento de Datos Personales al Responsable del Tratamiento en cualquier momento. Consulte la información de contacto que figura al principio de este documento.

**Derechos de los usuarios**
Los usuarios tienen derecho, en cualquier momento, a saber si sus Datos Personales han sido almacenados y pueden consultar al Responsable del Tratamiento para conocer su contenido y origen, verificar su exactitud o solicitar que se completen, cancelen, actualicen o rectifiquen, o su transformación en formato anónimo o el bloqueo de los datos conservados en violación de la ley, así como oponerse a su tratamiento por todos y cada uno de los motivos legítimos. Las solicitudes deben dirigirse al responsable del tratamiento a los datos de contacto indicados anteriormente.
Esta aplicación no admite solicitudes de "Do Not Track".
Para determinar si alguno de los servicios de terceros que utiliza respeta las solicitudes de "Do Not Track", lea sus políticas de privacidad.

**Cambios en esta política de privacidad**
El Responsable del tratamiento de los Datos se reserva el derecho a realizar cambios en esta política de privacidad en cualquier momento, avisando a sus usuarios en esta página. Se recomienda encarecidamente revisar esta página con frecuencia, consultando la fecha de la última modificación que figura en la parte inferior. Si un Usuario se opone a alguna de las modificaciones de la Política, deberá dejar de utilizar esta Aplicación y podrá solicitar al Responsable del Tratamiento la eliminación de los Datos Personales. A menos que se indique lo contrario, la política de privacidad vigente en ese momento se aplica a todos los Datos Personales que el Responsable del Tratamiento tiene sobre los Usuarios.

**Información sobre esta política de privacidad**
El Responsable del Tratamiento de los Datos es responsable de esta política de privacidad.

## Definiciones y referencias legales

Datos personales (o datos)
Cualquier información relativa a una persona física, una persona jurídica, una institución o una asociación, que sea o pueda ser identificada, incluso indirectamente, por referencia a cualquier otra información, incluido un número de identificación personal o token de acceso.

Datos de uso
Información recopilada automáticamente a partir de esta Aplicación (o de los servicios de terceros empleados en esta Aplicación), que puede incluir: las direcciones IP o los nombres de dominio de los ordenadores utilizados por los Usuarios que utilizan esta Aplicación, las direcciones URI (Uniform Resource Identifier), la hora de la solicitud, el método utilizado para enviar la solicitud al servidor, el tamaño del archivo recibido como respuesta, el código numérico que indica el estado de la respuesta del servidor (resultado satisfactorio, error, etc. ), el país de origen, las características del navegador y el sistema operativo utilizados por el usuario, los distintos datos de tiempo por visita (por ejemplo el tiempo empleado en cada página dentro de la Aplicación) y los detalles sobre la ruta seguida dentro de la Aplicación con especial referencia a la secuencia de páginas visitadas, y otros parámetros sobre el sistema operativo del dispositivo y/o el entorno informático del Usuario.

Usuario
La persona física que utiliza esta Aplicación, que debe coincidir con el Titular de los datos o estar autorizada por él, a quien se refieren los Datos Personales.

Titular de los datos
La persona física o jurídica a la que se refieren los Datos Personales.

Responsable del Tratamiento (o Propietario)
La persona física, la persona jurídica, la administración pública o cualquier otro organismo, asociación u organización con derecho, también conjuntamente con otro Controlador de Datos, a tomar decisiones sobre los fines, y los métodos de procesamiento de Datos Personales y los medios utilizados, incluyendo las medidas de seguridad relativas al funcionamiento y uso de esta Aplicación. El Controlador de Datos, a menos que se especifique lo contrario, es el Propietario de esta Aplicación.

Esta Aplicación
La herramienta de hardware o software mediante la cual se recogen los Datos Personales del Usuario.

Cookies
Pequeños datos almacenados en el dispositivo del Usuario.

Información legal
Aviso a los usuarios europeos: esta declaración de privacidad ha sido elaborada en cumplimiento de las obligaciones del Art. 10 de la Directiva CE n. 95/46/CE, y bajo las disposiciones de la Directiva 2002/58/CE, revisada por la Directiva 2009/136/CE, en materia de Cookies.
Esta política de privacidad se refiere únicamente a esta aplicación.
